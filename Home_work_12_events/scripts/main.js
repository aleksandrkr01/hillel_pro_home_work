"use strict";

// Практические задания.
// 1. Создать простой интерфейс модального окна. Создайте кнопку, при нажатии на которую открывается модальное окно. 
// Окно должно закрываться при нажатии на крестик закрытия, а так же при нажатии вне область окна.
// 2. Создайте группу аккордеонов (сворачивающиееся списки). У кажного списка есть заголовок при нажатии на который 
// список раскрывается. В один момент времени может существовать только один открытый список.

// Часть 1

var openPopup = document.querySelector('.popup-open');
var closePopup = document.querySelector('.popup-close');
var popup = document.querySelector('.popup-wrapper');
var popupWrapper = document.querySelector('.popup-wrapper');

// Открываем попап при клике на кнопку
openPopup.addEventListener('click', open);

// Закрываем попап при клике на кнопку
closePopup.addEventListener('click', close);

// Закрываем попап при клике на сторонее поле 
popup.addEventListener('click', close);

// Функция открытия попапа
function open(evt) {
	evt.preventDefault();
	popup.classList.remove('hidden');
};

// Закрытие попапа с определёнными evt.target, может и не правильно, зато удобно кастомизировать
// закрывашку 
function close(evt) {
	if(evt.target === popupWrapper || evt.target === closePopup) {
		evt.preventDefault();
		popup.classList.add('hidden');
	}
};

// Часть 2

var accordeonList = document.querySelector('.accordeon-list');
var accordeonTitle = document.querySelectorAll('.accordeon-title');

// Закрываем полностью аккордион при клике на список
accordeonList.addEventListener('click', function(evt) {
	if(evt.target === accordeonList) {
		closeAccordeon();
	}
});

// Каждому элементу списка при клике на него удаляем класс hidden
for(var i = 0; i < accordeonTitle.length; i++) {
	accordeonTitle[i].addEventListener('click', openAccordeon)
};

// Если у элемента списка нет класса hidden, то добавляем
function closeAccordeon(evt) {
	for(var i = 0; i < accordeonTitle.length; i++) {
		if(!accordeonTitle[i].nextElementSibling.classList.contains('hidden')) {
			accordeonTitle[i].nextElementSibling.classList.add('hidden');
		}
	}
};

// Открываем элемент по которому произошёл клик
function openAccordeon() {
	closeAccordeon();
	this.nextElementSibling.classList.remove('hidden');
};