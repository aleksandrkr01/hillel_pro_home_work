"use strict"
// Первое практическое задание:
// На декартовой системе координат размещена окружность так, что её центр совпадает с началом отсчёта осей. 
// Окружность вписана в квадрат. Стороны квадрата перпендикулярны осям (см. прикреплённое изображение). 
// Радиус окружности равен 5 единиц, а сторона квадрата - 10.
// Пользователь вводит две координаты точки с помощью prompt(). 
// Выясните и выведите в alert() находится ли эта точка в заштрихованой области.

// Task 1

var xCoord, yCoord, squereSide, circleRadius, checkOut;
xCoord = prompt("Введите x:");
yCoord = prompt("Введите y:");
squereSide = 10;
circleRadius = 5;
checkOut = Math.sqrt(Math.pow(xCoord, 2) + Math.pow(yCoord, 2));
if(xCoord === null || yCoord === null) {
	alert("Зря Вы не проверили");
}
// В строке ниже первых 2 условия проверяют не являются ли введённые значения пробелом или пустой
// строкой, вторые 2 условия не дают ввести выражения, типа: 1рр, 2ор и т.д.
else if(isNaN(parseFloat(xCoord)) || isNaN(parseFloat(yCoord)) || isNaN(xCoord) || isNaN(yCoord)) {
	alert("Координаты x и y должны быть числами");
}
else if(xCoord>0 && yCoord>0 && xCoord<squereSide && yCoord<squereSide && checkOut>circleRadius) {
	alert("Точка попадает в заштрихованную область");
}
else {
	alert("Точка не попадает в заштрихаванную область");
};

// Второе практическое задание:
// В Одессе открыли наземное метро и ввели тарифы на перевозку ручной клади. 
// Выделили три категории оплаты по массе:
// 1. 0 - 5 кг - 3 грн за килограмм.
// 2. 5 -10 кг - 5 грн за килограмм.
// 3. 10 -15 кг - 10 грн за килограмм.
// Через три дня приехал турист с Англии. Выведите в консоль сколько он должен заплатить фунтов стерлингов 
// за свою кладь, массу которой он вводит в prompt(), с учётом, что он вводит её в фунтах. 
// Курс валюты на момент ввода фиксированный и составляет 2.88 фунтов стерлингов за 100 украинских гривен.
// 1 кг = 2,20462 фунтов
// Учтите, что он джентельмен и может ввести массу прописью. 
// Уведомьте его о том, что можно вводить только числа.
// P. S: Воспользуйтесь творческим порывом Андрея Герасименко для проверки на число.

// Task 2

var amount, massage;
var currency = "GBP";
var courseForHundredHryvnas = 2.88;
var weightIndex = 2.20462;
var inputWeight = +prompt("Please enter your luggage weight in pounts (number only):");
var lowTax = 3*(courseForHundredHryvnas/100);
var mediumTax = 5*(courseForHundredHryvnas/100);
var highTax = 10*(courseForHundredHryvnas/100);
var lowBorder = 5;
var mediumBorder = 10;
var highBorder = 15;
var outputWeight = inputWeight / weightIndex;
if(outputWeight>highBorder) {
	massage = `Please, take your luggage to the luggage compartment.`;
}
else if(outputWeight<0) {
	massage = `Please enter the number, that is equel to weight of your luggage, Ser.`;
}
else {
	(outputWeight>mediumBorder) ? amount = outputWeight*highTax:
	(outputWeight>lowBorder) ? amount = outputWeight*mediumTax: amount = outputWeight*lowTax;
	isNaN(amount) ? amount = "incalculable": amount = amount.toFixed(2);
	massage = `Sum of tax - ${amount} ${currency}`;
}
console.log(massage);

// Постаралcя по максимуму изменить алгоритм. Оставил запись if в развёрнутом виде для большей наглядности, переменные налога и лимитов нужны для 
// избавления от магических чисел. Переменную massage оставлю для того, чтобы в каждом условии не обращаться в консоль. Итого код соератил на 11
// строк. И да, за тернарник в блоке else для меня уже, наверное, в аду котёл готовят, но надеюсь на практие это лучше, чем вложенный if