"use strict";

// Для редактироваия банкоматов данные админа в объекте adminData;s

document.ready = (function() {

	let str = 'ОСТАНОВИТЕСЬ!!!!!!!!!!        ';
	let str2 = 'Консоль предназначена для разработки, а не для взлома банкомата.'

	console.log ( '%c%s', 'color: red; font: 3.2rem/1 Tahoma; ' +
	'background-image: url("https://www.meme-arsenal.com/memes/9e5f17aeee92343752f740cf1c4b3b55.jpg"); ' +
	'background-repeat: no-repeat; background-size: 30%; padding-left: 500px', str + str2);
	
	let tariffs = serverTariffs;
	let base = datsBase;

	let atmOutput = document.querySelector('#atm__output');

	let ATMS = ATMList;
	let ATMItem;

	let mode;

	let cardNumber = document.createElement('input');
	let cardPin = document.createElement('input');

	let targetClient;
	let targetCard;

	let ATMOutputList = document.createElement('ul');

	let startButton = document.querySelector('#start');

	startButton.addEventListener('click', function(event) {
		event.preventDefault();
		mode  = 'user';
		initATMS();
		startButton.classList.add('hidden');
	});

	function initATMS() {

		clearBlock(ATMOutputList);
		clearBlock(atmOutput);

	  for(let i = 0; i < ATMS.length; i++) {
	    let adress = ATMS[i].adress;
	    let comission = ATMS[i].comission;
	    let img = ATMS[i].img || 'img/mock.jpg';
	    let resultId = ATMS[i].id || defineId();

	    ATMItem = new Atm(adress, comission, resultId, img);
	    ATMItem.createATM(adress, comission, resultId, img);

	    ATMS[i] = ATMItem;
	  };
	};

	function clearBlock(elem) {
		while(elem.lastChild) {
			elem.removeChild(elem.lastChild);
		};
	};

	function defineId() {
	  let date = new Date;
	  let hash = date.getTime();
	  let autoId = 'ATM: ' + hash;
	  return autoId;
	};

	function isNumber(number) {
		return !isNaN(parseFloat(number)) && isFinite(number);
	};

	let Atm = function(adress, comission, id, img) {

		let self = this;
		let adminData = {
			card: 1111111111111111,
			pin: 1111,
		};

		let ATMTitle = document.createElement('h3');
		let ATMPhoto = document.createElement('img');
		let chooseButton = document.createElement('a');
		let removeButton = document.createElement('button');

		let atmAdress = document.createElement('input');
		let atmComission = document.createElement('input');

		let ATMId;
		let ATMAdress = '';
		let ATMComission;
		let ATMImg;

		let messageContainer = document.createElement('p');

		let cardForm = document.createElement('form');

		function renderATMS() {

			let ATMListItem = document.createElement('li');
			let photoInfo = document.createElement('span');
			
			ATMOutputList.classList.add('atm__list');
			ATMTitle.classList.add('atm__title');
			ATMListItem.classList.add('atm__item');
			ATMPhoto.classList.remove('atm__photo--mode');
			ATMPhoto.classList.add('atm__photo');
			photoInfo.classList.add('atm__info');

			photoInfo.textContent = 'Нажмите на изображение для его изменения';

			chooseButton.classList.add('atm__choose', 'button');
			removeButton.classList.add('atm__remove', 'button--remove');

			ATMTitle.textContent = ATMAdress;
			ATMPhoto.src = ATMImg;
			chooseButton.href = '#';
			chooseButton.textContent = 'Выбрать';
			removeButton.textContent = 'Удалить банкомат?';
			removeButton.setAttribute('data-id', ATMId);
			
			ATMListItem.appendChild(ATMTitle);
			ATMListItem.appendChild(ATMPhoto);
			ATMListItem.appendChild(chooseButton);
			ATMListItem.appendChild(removeButton);

			ATMOutputList.insertBefore(ATMListItem, ATMOutputList.children[0]);
			
			atmOutput.appendChild(ATMOutputList);

			if(mode === 'admin') {
				ATMPhoto.addEventListener('click', changeAvatar);
				ATMPhoto.classList.add('atm__photo--mode');
				ATMListItem.appendChild(photoInfo);
			};

			function changeAvatar(outerEvt) {
			  let input = document.createElement('input');
			  input.type = 'file';
			  input.accept = 'image/jpeg, image/png, image/bmp';
			  input.click();
			  input.addEventListener('change', function(innerEvt) {
			    let file = innerEvt.target.files[0];
			    changeAvatar.image = URL.createObjectURL(file); 
			    if(file.type.match(/image.*/)) {
			      outerEvt.target.setAttribute('src', URL.createObjectURL(file));
			      self.img = URL.createObjectURL(file);
			    }
			    else alert('Wrong file type');
			  });
			};

			removeButton.addEventListener('click', function(event) {
				event.preventDefault();
				let confirmDelition = new Modal('Удалить банкомат?', 'confirm', atmOutput, removeATM);
				function removeATM() {
					let target = event.target;
					let id = target.getAttribute('data-id');
					ATMList = ATMList.filter(function(item) {
						return item.id !== id;
					});
					target.parentNode.remove();
					ATMS = ATMList;
				}
			});

			switch(mode) {
				case 'user':
					removeButton.classList.add('hidden');
					chooseButton.classList.remove('hidden');
				break;
				case 'admin':
					removeButton.classList.remove('hidden');
					chooseButton.classList.add('hidden');
				break;
			};
			chooseButton.addEventListener('click', function(event) {
				event.preventDefault();
				renderWelcomePage();
			});
		};

		function renderWelcomePage() {

			let welcomePage = document.createElement('div');
			let welcomeTitle = document.createElement('p');
			let welcomeButton = document.createElement('a');
			let welcomeImg = document.createElement('img');

			welcomePage.classList.add('atm__welcome', 'welcome');

			welcomeTitle.classList.add('welcome__title');
			welcomeTitle.textContent = 'Нажмите на изображение для продолжения';

			welcomeButton.classList.add('welcome__button');
			welcomeButton.id = 'welcome__button';
			welcomeButton.setAttribute('href', '#');

			welcomeImg.classList.add('welcome__image');
			welcomeImg.setAttribute('alt', 'welcome');
			welcomeImg.setAttribute('src', 'img/welcome.png');

			clearBlock(atmOutput);

			welcomePage.appendChild(welcomeTitle);
			welcomePage.appendChild(welcomeButton);
			welcomeButton.appendChild(welcomeImg);
			atmOutput.appendChild(welcomePage);

			welcomeButton.addEventListener('click', renderStart);
		};

		function renderStart(event) {

			event.preventDefault();
			
			let cardSubmit = document.createElement('button');
			cardNumber.value = '';
			cardPin.value = '';

			cardForm.classList.add('card__form');
			cardNumber.classList.add('card__number', 'atm__input');
			cardPin.classList.add('card__pin', 'atm__input');
			cardSubmit.classList.add('card__submit', 'button');

			cardNumber.type = 'text';
			cardNumber.name = 'cardNumber';
			cardNumber.placeholder = 'Введите номер карты';

			cardPin.type = 'password';
			cardPin.name = 'cardPin';
			cardPin.placeholder = 'Введите PIN-код';

			cardSubmit.type = 'submit';
			cardSubmit.textContent = 'Подтвердить данные';

			clearBlock(atmOutput);

			getBack();

			cardForm.appendChild(cardNumber);
			cardForm.appendChild(cardPin);
			cardForm.appendChild(cardSubmit);
			atmOutput.appendChild(cardForm);

			cardNumber.focus();
			cardSubmit.disabled = true;
			cardNumber.addEventListener('input', checkBlock);
			cardPin.addEventListener('input', checkBlock);

			function checkBlock(event) {
				event.preventDefault();
				if(isValidData(cardNumber, cardPin)) {
					cardSubmit.disabled = false;
				}
				else {
					cardSubmit.disabled = true;
				};
			};

			cardSubmit.addEventListener('click', function(event) {
				event.preventDefault();
				if(adminData.card === Number(cardNumber.value) && adminData.pin === Number(cardPin.value)) {
					renderCreatePanel();
				}
				else{
					defineClient();
				};
			});
			
			cardForm.addEventListener('keypress', setNumeric);
		};

		function setNumeric(event) {
			let numberMinCode = 48;
			let numberMaxCode = 57;
			let numberPadMinCode = 96;
			let numberPadMaxCode = 105;
			let backspaceCode = 8;
			if((event.which < numberMinCode || event.which > numberMaxCode) || ((event.which > numberMaxCode && event.which < numberPadMinCode) || event.which > numberPadMaxCode) && event.which !== backspaceCode) {
				event.preventDefault();
			};
		};

		function setStatus(message, type, parent, autoclose, time = 2000) {

			messageContainer.textContent = message;
			switch(type) {
				case 'success':
					messageContainer.classList = '';
					messageContainer.classList.add('message', 'message--sгccess');
				break;
				case 'error':
					messageContainer.classList = '';
					messageContainer.classList.add('message', 'message--error');
				break;
				case 'warning':
					messageContainer.classList = '';
					messageContainer.classList.add('message', 'message--warning');
				break;
			}
			parent.appendChild(messageContainer);
			if(autoclose) {
				setTimeout(() => {
				  parent.removeChild(messageContainer);
				}, time);
			}
		};

		function isValidData(number, pin) {
				let numberLength = 16;
				let pinLength = 4;
				let userNumber = number.value;
				let userPin = pin.value;

				number.setAttribute('maxLength', numberLength);
				pin.setAttribute('maxLength', pinLength);

				if(isNumber(userNumber) && userNumber.length === numberLength) {
					pin.focus();
					if(isNumber(userPin) && userPin.length === pinLength) {
						return true;
					}
				}
				else {
					return false;
				}		
		};

		function getBack() {
			startButton.textContent = 'Назад к выбору банкоматов';
			startButton.classList.remove('hidden');
		};

		function renderCreatePanel() {

			let adminPanel = document.createElement('form');
			let createButton = document.createElement('button');

			getBack();

			atmAdress.type = 'text';
			atmAdress.placeholder = 'Введите адрес';
			atmAdress.type = 'text';
			atmComission.placeholder = 'Введите комиссию';

			createButton.textContent = 'Создать';

			adminPanel.classList.add('admin');
			atmAdress.classList.add('admin__adress', 'atm__input');
			atmComission.classList.add('admin__comossion', 'atm__input');
			createButton.classList.add('admin__button', 'button--add');

			mode = 'admin';
			initATMS(event);

			atmOutput.insertBefore(adminPanel, atmOutput.firstChild);
			adminPanel.appendChild(atmAdress);
			adminPanel.appendChild(atmComission);
			adminPanel.appendChild(createButton);

			createButton.addEventListener('click', function(event) {

				event.preventDefault();
				let adress = atmAdress.value;
				let comission = atmComission.value;
				let id = defineId();
				let img = 'img/mock.jpg';

				let maxComission = 5;
				if(isNumber(comission) && comission <= maxComission) {
					ATMItem = new Atm(adress, comission, id, img);
					ATMItem.createATM(adress, comission, id, img);
					ATMS.unshift(ATMItem);
				}
				else {
					let comissionModal = new Modal('Комиссия должна быть числом меньше ' + maxComission + '.', 'alert', atmOutput);
					atmComission.value = '';
					atmComission.focus();
					return;
				};

				atmAdress.value = '';
				atmComission.value = '';
			});
		};

		function renderUserPanel() {

			clearBlock(atmOutput);

			let buttonsBlock = document.createElement('div');
			let balanceBtn = document.createElement('button');
			let withdrawalBtn = document.createElement('button');
			let changeNumberBtn = document.createElement('button');
			let chagePin = document.createElement('button');

			balanceBtn.textContent = 'Проверить баланс';
			withdrawalBtn.textContent = 'Снять наличные';
			changeNumberBtn.textContent = 'Поменять номер телефона';
			chagePin.textContent = 'Сменить ПИН';

			buttonsBlock.appendChild(balanceBtn);
			buttonsBlock.appendChild(withdrawalBtn);
			buttonsBlock.appendChild(changeNumberBtn);
			buttonsBlock.appendChild(chagePin);

			balanceBtn.classList.add('button');
			withdrawalBtn.classList.add('button');
			changeNumberBtn.classList.add('button');
			chagePin.classList.add('button');

			balanceBtn.setAttribute('data-action', 'checkBalance');
			withdrawalBtn.setAttribute('data-action', 'renderWithdrawal');
			changeNumberBtn.setAttribute('data-action', 'changePhone');
			chagePin.setAttribute('data-action', 'changePin');

			atmOutput.appendChild(buttonsBlock);

			let menu = new ATMMenu();

			buttonsBlock.addEventListener('click', function(event) {
				let target = event.target;
      	let action = target.getAttribute('data-action');
	      if (action) {
	        menu[action]();
	      };
			});	
		};

		function defineClient() {

			let inputCard = +cardNumber.value;
			let inputPin = +cardPin.value;
			let maxPinCount = 2;
			
			for(let i = 0; i < base.length; i++) {
				let client = base[i];
				for(let j = 0; j < base[i].cards.length; j++) {
					var card = client.cards[j];
					if(card.cardNumber === inputCard && card.pin === inputPin && !card.cardLocked) {
						targetClient = base[i];
						targetCard = base[i].cards[j];
						renderUserPanel();
						return;
					}
					else if(card.cardNumber === inputCard && card.pin !== inputPin && !card.cardLocked) {
						++card.wrongPinCount;
						let wrongPinInfo = new Modal('Вы ввели пин неправильно ' + card.wrongPinCount + ' раз. Продолжить?', 'confirm', atmOutput, pinInfoCallback, pinCancelCallback);
						function pinInfoCallback() {
							cardPin.value = '';
							cardPin.focus();
						};
						function pinCancelCallback() {
							cardNumber.value = '';
							cardPin.value = '';
						};
						if(card.wrongPinCount > maxPinCount) {
 							card.cardLocked = true;
						} 
						return;
					}
					else if(card.cardNumber === inputCard && card.cardLocked) {
						let lockAlert = new Modal('Карта заблокирована', 'alert', atmOutput);
						mode  = 'user';
						cardNumber.value = '';
						cardPin.value = '';
						return;
					};
				};
			};
			if(card.cardNumber !== inputCard) {
				let wrongCardAlert = new Modal('Неверный номер карты', 'alert', atmOutput);
				cardNumber.value = '';
				cardPin.value = '';
				return;
			};
		};

		let ATMMenu = function() {

			let ounBalance = Number(targetCard.balance);
			let creditBalance;
			let availableBalance;

			if(targetCard.tariffs.credit) {
				creditBalance = Number(targetCard.limit);
			}
			else {
				creditBalance = 0;
			};

			availableBalance = ounBalance + creditBalance;

			this.checkBalance = function() {

				let ounAmount = document.createElement('span');
				let creditLimit = document.createElement('span');
				let availableMoney = document.createElement('span');
				let balanceBlock = document.createElement('p');

				let balance = new Modal(balanceBlock, 'alert', atmOutput);

				ounAmount.textContent = 'Ваши средства: ' + ounBalance.toFixed(2) + ' грн. ';
				creditLimit.textContent = 'Доступный кредитны лимит: ' + creditBalance.toFixed(2) + ' грн. ';
				availableMoney.textContent = 'Всего доступно: ' + availableBalance.toFixed(2) + ' грн. ';

				ounAmount.classList.add('balance-string');
				creditLimit.classList.add('balance-string');
				availableMoney.classList.add('balance-string');
				balanceBlock.classList.add('balance-block');

				balanceBlock.appendChild(ounAmount);
				balanceBlock.appendChild(creditLimit);
				balanceBlock.appendChild(availableMoney);
			};

			this.renderWithdrawal = function() {

				let withdrawalAmount = document.createElement('input');
				let withdrawalButton = document.createElement('button');
				let withdrawalBlock = document.createElement('div');
				let withdrawalWindow = new Modal(withdrawalBlock, 'alert', atmOutput);

				withdrawalAmount.placeholder = 'Введите сумму';
				withdrawalButton.textContent = 'Подтвердить снятие';

				withdrawalAmount.classList.add('atm__input');
				withdrawalButton.classList.add('button');
				
				withdrawalButton.disabled = true;

				withdrawalAmount.addEventListener('keypress', setNumeric);

				withdrawalBlock.appendChild(withdrawalAmount);
				withdrawalBlock.appendChild(withdrawalButton);

				withdrawalAmount.autofocus = true;

				withdrawalAmount.addEventListener('input', function() {
					if(withdrawalAmount.value) {
						withdrawalButton.disabled = false;
					}
					else {
						withdrawalButton.disabled = true;
					}	
				});

				withdrawalButton.addEventListener('click', withdrawalCallback);

				function withdrawalCallback(event) {
					event.preventDefault();

					let moneyValue = withdrawalAmount.value;
					let cardComissionRate = targetCard.tariffs.commission;
					let ATMComissionRate = self.comission;
					let comissionRate = cardComissionRate + ATMComissionRate;
					let comissionValue = Number((moneyValue * (comissionRate/100)).toFixed(2));
					let withdrawalBalance = +moneyValue + +comissionValue;

					let message;

					function annullField() {
						withdrawalAmount.focus();
						withdrawalAmount.value = '';
						withdrawalButton.disabled = true;
					};
					
					if(withdrawalBalance > availableBalance) {
						setStatus('Недостаточно средств!!!', 'error', withdrawalBlock, true);
						annullField();
						return;
					}

					switch(true) {
						case (ounBalance>0):
							ounBalance -= withdrawalBalance;
						break;
						case (creditBalance>0):
							creditBalance -= withdrawalBalance;
						break;
					};

					targetCard.balance = Number(ounBalance.toFixed(2));
					targetCard.limit = Number(creditBalance.toFixed(2));

					availableBalance = ounBalance + creditBalance;

					message = 'Вы сняли: ' + moneyValue + ' грн. Всего доступно к снятию: ' + availableBalance.toFixed(2) + ' грн.';
					setStatus(message, 'success', withdrawalBlock);

					annullField();
				};
			};

			this.changePhone = function() {
				let phoneInput = document.createElement('input');
				let phoneButton = document.createElement('button');
				let phoneBlock = document.createElement('div');

				let phoneLength = 10;
				let phoneWindow = new Modal(phoneBlock, 'alert', atmOutput);

				phoneBlock.appendChild(phoneInput);
				phoneBlock.appendChild(phoneButton);

				phoneInput.placeholder = 'Введите номер';
				phoneInput.autofocus = true;

				phoneInput.classList.add('atm__input');
				phoneButton.classList.add('button');

				phoneButton.textContent = 'Изменить номер';

				phoneButton.disabled = true;

				phoneInput.addEventListener('input', function(event) {
					event.preventDefault();
					if(checkPhone(phoneInput)) {
						phoneButton.disabled = false;
					}
					else phoneButton.disabled = true;
				});
				
				phoneInput.setAttribute('maxLength', phoneLength);
				phoneInput.addEventListener('keypress', setNumeric);

				phoneButton.addEventListener('click', phoneCallback);

				function phoneCallback(event) {
					event.preventDefault();
					let usersPhone = phoneInput.value;
					let message;
					targetClient.phoneNumber = usersPhone;
					message = 'Вы успешно сменили номер на: ' + usersPhone;
					setStatus(message, 'success', phoneBlock);
					phoneInput.value = '';
					phoneInput.focus();
					phoneButton.disabled = true;
				};

				function checkPhone(inputtxt) {
					var phoneno = /^\(?([0]{1})([0-9]{2})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
				  if(inputtxt.value.match(phoneno)) {
				    return true;
				  }
				  else {
				    return false;
				  };
				};
			};

			this.changePin = function() {

				let oldPinInput = document.createElement('input');
				let newPinInput = document.createElement('input');
				let confirmPinInput = document.createElement('input');
				let pinButton = document.createElement('button');
				let pinBlock = document.createElement('div');
				let inputsBlock = document.createElement('div');

				let pinLength = 4;

				let pinWindow = new Modal(pinBlock, 'alert', atmOutput);

				inputsBlock.appendChild(oldPinInput);
				inputsBlock.appendChild(newPinInput);
				inputsBlock.appendChild(confirmPinInput);
				pinBlock.appendChild(inputsBlock);
				pinBlock.appendChild(pinButton);
				
				oldPinInput.placeholder = 'Введите старый PIN-код';
				newPinInput.placeholder = 'Введите новый PIN-код';
				confirmPinInput.placeholder = 'Подтвердите PIN-код';

				pinBlock.classList.add('input__pin');
				inputsBlock.classList.add('input__block');
				oldPinInput.classList.add('atm__input');
				newPinInput.classList.add('atm__input');
				confirmPinInput.classList.add('atm__input');
				pinButton.classList.add('button');

				oldPinInput.type = 'password';
				newPinInput.type = 'password';
				confirmPinInput.type = 'password';

				oldPinInput.setAttribute('maxLength', pinLength);
				newPinInput.setAttribute('maxLength', pinLength);
				confirmPinInput.setAttribute('maxLength', pinLength);

				pinButton.textContent = 'Сменить PIN';

				oldPinInput.autofocus = true;

				pinButton.disabled = true;

				pinBlock.addEventListener('input', function(event) {
					event.preventDefault();
					if(+oldPinInput.value.length === pinLength && +newPinInput.value.length === pinLength && +confirmPinInput.value.length === pinLength) {
						pinButton.disabled = false;
						pinButton.focus();
					}
					else pinButton.disabled = true;
				});

				pinBlock.addEventListener('keypress', setNumeric);

				focusNext(oldPinInput, 4);
				focusNext(newPinInput, 4);

				pinButton.addEventListener('click', pinChangeCallback);

				function pinChangeCallback(event) {

					event.preventDefault();

					let oldPin = +oldPinInput.value;
					let newPin = +newPinInput.value;
					let confirmPin = +confirmPinInput.value;

					if(+oldPin === targetCard.pin && newPin === confirmPin) {
						targetCard.pin = newPin;
						setStatus('PIN-код изменён успешно', 'success', pinBlock, true);
						annulPin();
					}
					else if(+oldPin !== targetCard.pin) {
						setStatus('Вы ввели неверный PIN-код', 'error', pinBlock, true);
						annulPin();
						return;
					}
					else if(newPin !== confirmPin) {
						setStatus('Новый пин и подтверждение не совпадают', 'warning', pinBlock, true);
						annulPin();
						return;
					};

					function annulPin() {
						oldPinInput.value = '';
						newPinInput.value = '';
						confirmPinInput.value = '';
						oldPinInput.focus();
						pinButton.disabled = true;
					};
				};

				function focusNext(element, length) {
					element.addEventListener('input', function() {
						if(element.value.length === length) {
							element.nextSibling.focus();
						}
					});				
				};
			};
		};

		this.id = id;
		this.adress = adress;
		this.comission = comission;
		this.img = img;	

		this.createATM = function(adress, comission, id, img) {
			ATMId = id;
			ATMAdress = adress;
			ATMComission = comission;
			ATMImg = img;
			renderATMS();
		};
	};

	let Modal = function(message, type, parent, successCb, cancelCb) {

		let modalBlock = document.createElement('div');
		let modalWrepper = document.createElement('div');
		let messageContainer = document.createElement('div');
		let btnBlock = document.createElement('div');
		let closeBtn = document.createElement('button');
		let confirmBtn = document.createElement('button');
		let self = this;

		if(typeof message === 'object') {
			messageContainer.appendChild(message)
		}
		else {
			messageContainer.textContent = message;
		};
		
		confirmBtn.textContent = 'Подтвердить';

		modalBlock.appendChild(modalWrepper);

		modalWrepper.appendChild(messageContainer);
		modalWrepper.appendChild(btnBlock);

		btnBlock.appendChild(closeBtn);
		parent.insertBefore(modalBlock, parent.firstChild);

		modalBlock.classList.add('modal');
		modalWrepper.classList.add('modal__wrapper');
		messageContainer.classList.add('modal__message');
		closeBtn.classList.add('button', 'modal__button', 'button--cancel')
		confirmBtn.classList.add('button', 'modal__button', 'button--confirm')

		function close(parent) {
			parent.removeChild(modalBlock);
		};

		switch(type) {
			case 'confirm':
				btnBlock.appendChild(confirmBtn);
				closeBtn.textContent = 'Отменить';
			break;
			case 'alert':
				closeBtn.textContent = 'Закрыть';
			break;
		}

		confirmBtn.addEventListener('click', function(event) {
			event.preventDefault();
			close(parent);
			successCb();
		});

		closeBtn.addEventListener('click', function(event) {
			event.preventDefault();
			close(parent);
			if(typeof cancelCb !== 'undefined') {
				cancelCb();
			};
		});	
	};

})();
