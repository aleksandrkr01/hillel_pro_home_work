"use strict";

let datsBase = [
   {
      id: 12345,
      phoneNumber: "0667658943",
      cards: [
         {
            cardNumber: 2222222222222222,
            tariffs: serverTariffs.creditStandart,
            limit: 3000,
            balance: -1500,
            pin: 2222,
            wrongPinCount: 0,
            cardLocked: false
         },
         {
            cardNumber: 4444444444444444,
            tariffs: serverTariffs.debitStandart,
            balance: 2156.64,
            pin: 4444,
            wrongPinCount: 0,
            cardLocked: false
         }
      ]
   },
   {
      id: 65443,
      phoneNumber: "0662334456",
      cards: [
         {
            cardNumber: 4546789765443345,
            tariffs: serverTariffs.creditGold,
            limit: 3000,
            balance: -1479,
            pin: 3333,
            wrongPinCount: 0,
            cardLocked: false
         }
      ]
   }
]
