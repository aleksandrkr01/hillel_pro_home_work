"use strict";

let serverTariffs = {
   debitStandart: {
      credit: false,
      commission: 3
   },
   creditStandart: {
      credit: true,
      commission: 3
   },
   debitGold: {
      credit: false,
      commission: 2
   },
   creditGold: {
      credit: true,
      commission: 2
   }
}

