"use strict";

// Напишите функцию-скраппер (https://en.wikipedia.org/wiki/Web_scraping), которая принимает часть и главу 
// учебника со страницы http://learn.javascript.ru/ и выводит в консоль список тем этой главы. Сделайте возможность 
// выбора стиля вывода третим параметром функции ('number' - нумерация, 'dash' - дефис, 'current' - нумерация, как в 
// учебнике с сохранинием порядка). Не бойтесь эксперементировать со стилизацией console.log() 
// (https://developer.mozilla.org/en-US/docs/Web/API/console#Outputting_text_to_the_console).

// Результат выполненного задания - я открываю страницу http://learn.javascript.ru/, вставляю ваш код, 
// выбираю часть, главу и вид и получаю список тем.

// ** Выведите список в консоль в таблице. (не обязательно)

function getChapters (part, chapter, style) {
	var parts = document.querySelectorAll('.list');
	var chapters = document.querySelectorAll('.list__item');
	var themes = document.querySelectorAll('.list-sub__link');
	var partContent = [];
	var outputThemes = {};
	var chapterCounter = count(1);
	var pointCounter = count(1);
	part--;
	chapter--;
	if(part<0 || part>parts.length-1) {
		console.warn(`Введите номер части от 1 до ${parts.length}`);
		return;
	}
	for(var i=0; i<parts.length; i++) {
		partContent[i] = [];
		for(var j=0; j<chapters.length; j++) {
			if(parts[i].contains(chapters[j])) {
				partContent[i].push(chapters[j]);
			};
		};
	};
	if(chapter<0 || chapter>partContent[part].length-1) {
		console.warn(`Введите номер главы от 1 до ${partContent[part].length}`);
		return;
	}
	for(var i = 0; i < themes.length; i++) {
		if(partContent[part][chapter].contains(themes[i])) {
			(style === 'number' || style === 'current')? outputThemes[setStyle(style, chapterCounter(), chapter)] = `${themes[i].textContent}`:
			outputThemes[`Пункт ${pointCounter()}`] = `${setStyle(style, chapterCounter(), chapter)} ${themes[i].textContent}`;
		};
	};
	console.table(outputThemes);
};

function setStyle(style, callback, chapter) {
	var prefix;
    chapter++;
	switch(style) {
		case 'dash':
			prefix = '-';
		break;
		case 'number': 
			prefix = callback;
		break;
		case 'current': 
			prefix = chapter + '.' + callback;
		break;
		default:
			prefix = '\u0F04';
	} 
	return prefix;
};

function count(startCount) {
	var counter = startCount;
	return function() {
		return counter++;
	};
};

getChapters(1, 1, 'dash');
