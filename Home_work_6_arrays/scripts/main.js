"use strict";
// Блок кнопок на 1 и 2 задание
var task1 = document.querySelector(".btn-1");
var task2_1 = document.querySelector(".btn-2-1");
var task2_2 = document.querySelector(".btn-2-2");

// Объекты для второго задания
var firstCheck = checkElement();
var secondCheck = checkElement([1, 2, 3, 5, "h"]);

// Обработчики для 1 и 2 заданий
task1.addEventListener("click", setTask);
task2_1.addEventListener("click", firstCheck);
task2_2.addEventListener("click", secondCheck);

// Просто функция проверки на число
function isNumber(number) {
	return !isNaN(parseFloat(number)) && isFinite(number);
}

// Кнопки для корзины
var cartForm = document.querySelector(".cart__btns-wrapper");
var buttons = document.querySelectorAll("button");
var cartFill = document.querySelector(".btn__fill");
var sortIncrease = document.querySelector(".btn__increase");
var sortDecline = document.querySelector(".btn__decline");
var filterCategory = document.querySelector(".btn__category");
var filterPrice = document.querySelector(".btn__price");
var cartFilter = document.querySelector(".btn__filter");
var categoryQuantity = document.querySelector(".btn__quantity");
var categryPrice = document.querySelector(".btn__total-price");
var cartDelete = document.querySelector(".btn__delete");
var cartClear = document.querySelector(".btn__clear");

// Создаём корзину
var myCart = gedCart();

// Обработчики кнопок корзины
// Удаляем поведение по умолчанию со всех кнопок
buttons.forEach(function(button) {
	button.addEventListener("click", function(evt) {
		evt.preventDefault();
	});
});
// Заполняем корзину
cartFill.addEventListener("click", myCart.fillCart);
// Выбираем сортировку и активируем фильтр
cartForm.addEventListener("change", function(evt) {
	evt.preventDefault();
	let firstFilter = false;
	let secondFilter = false;
	if (sortIncrease.checked) {
		myCart.sortToMost();
		firstFilter = true;
	}
	if (sortDecline.checked) {
		myCart.sortToLess();
		firstFilter = true;
	}
	if (filterPrice.checked) {
		myCart.priceFilterCart();
		secondFilter = true;
	}
	if (filterCategory.checked) {
		myCart.categoryFilterCart();
		categoryQuantity.removeAttribute("disabled");
		secondFilter = true;
	} else {
		categoryQuantity.setAttribute("disabled", "true");
	}
	if (secondFilter) {
		categryPrice.removeAttribute("disabled");
		if (firstFilter) {
			cartFilter.removeAttribute("disabled");
		}
	}
});
// Узнаём ширину категории
categoryQuantity.addEventListener("click", myCart.categoryFiltredQuantity);
// Узнаём цену отфильтрованых товаров
categryPrice.addEventListener("click", myCart.getPriceSum);
// Фильтруем корзину по выбраным выше параметрам
cartFilter.addEventListener("click", myCart.filter);
// Удаляем элемент по имени
cartDelete.addEventListener("click", myCart.removeByName);
// Очищаем корзину
cartClear.addEventListener("click", myCart.getClear);

