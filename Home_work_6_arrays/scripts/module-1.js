"use strict";
// 1. Пользователь вводит в prompt() числа через запятую (один раз много цифр, например: "54, 45, 53453, 7, 32").
// - Разрешите пользователю вводить минимум три числа, максимум - десять.
// - Разрешите вводить только числа меньше ста.
// - Если всё введено верно, отсортируйте числа от меньшего к большему и выведите в консоль новую строку.
// * Усложнение (необязательно к выполнению): Отсортируйте числа на сначала чётные, потом нечётные.

// Честно, пока не знаю насколько правильно делать такой водопад функций, где в одной главной
// функции соберается работа всех вспомогательных, но надеюсь ход мыслей правильный
var setTask = function () {
	var numbersArr = getNumbers();
	if(!numbersArr) return;
	var currentMinLength = 3;
	var currentMaxLength = 10;
	var currentMinValue = 0;
	var currentMaxValue = 100;
	if (isValidLength (numbersArr, currentMinLength, currentMaxLength) && 
		isValidNumber (numbersArr, currentMinValue, currentMaxValue)) {
		var sortedNumbersString = sortNumbers(numbersArr).join(", ");
		var sortEvenOddString = sortEvenOdd(numbersArr).join(", ");
		renderTask(
			`Сотритовка от меньшего к большему: ${sortedNumbersString}`,
			`Сортировка парные-непарные: ${sortEvenOddString}`);
		console.log(sortedNumbersString);
		console.log(sortEvenOddString);
	}
	function getNumbers () {
		var numbers = prompt("Введите числа через запятую");
		if(numbers === null) return;
		numbers = numbers.replace(/, /g, ",");
		var numbersArray = numbers.split(",");
		return numbersArray;
	}
	function isValidLength (array, minLength, maxLength) {
		if(array.length<minLength || array.length>maxLength) {
			alert(`Допутимое количество цифер от ${minLength} до ${maxLength}`);
			return false;
		}
		return true;
	}
	function isValidNumber (array, minValue, maxValue) {
		for(var i = 0; i<array.length; i++) {
			array[i] = +array[i];
			if(array[i]<minValue || array[i]>=maxValue || !isNumber(array[i])) {
				alert(`Элемент №${++i} не входит в допустимый придел ${minValue} - ${--maxValue} или не является числом`);
				return false;
			}
		}
		return true;
	}
	function sortNumbers (array) {
		var compare = function (a, b) {
			if(a > b) return 1;
			if(a < b) return -1;
		};
		var sortedArr = array.sort(compare);
		return sortedArr;
	}
	function sortEvenOdd (array) {
		var evens = [];
		var odds = [];
		for(var i = 0; i<array.length; i++) {
			array[i]%2 === 0 ? evens.push(array[i]): odds.push(array[i]);
		}
		var sortedArr = evens.concat(odds);
		return sortedArr;
	}
	function renderTask() {
		var output = document.querySelector(".output");
		var list = document.createElement("ul");
		var listItem;
		for (var i = 0; i<arguments.length; i++) {
			listItem = document.createElement("li");
			listItem.innerText = arguments[i];
			list.appendChild(listItem);
		}
		output.appendChild(list);
	}
};