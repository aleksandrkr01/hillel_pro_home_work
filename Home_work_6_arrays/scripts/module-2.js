"use strict";
// 2. Есть массив [3,, 6,,, 7,, undefined, 34,,,, 97,, 43].
// - Пользователь вводит индекс. Ответьте есть ли элемент c таким индексом и, если есть, выведите его в 
// консоль. Так же перенесите его (элемент) в конец массива.

var checkElement = function (array) {
	var arr = [3,, 6,,, 7,, undefined, 34,,,, 97,, 43];
	(typeof array === "undefined") ? array = arr: array = array;
	return function () {
		var userIndex = prompt("Введите индекс");
		var massage;
		if (isNumber(userIndex) && userIndex>=0) {
			if(userIndex in array) {
				massage = `Элемент под индексом ${userIndex} есть в массиве, его значение ${array[userIndex]}`;
				array.push(array[userIndex]);
				array.splice(userIndex, 1);
				console.log(array);
			}
			else {
				massage = "Такого элемента нет в массиве";
			}
			console.log(massage);
		}
		else {
			console.log("Индекс введён некорректно");
		}
	};
};
