// 3. Напишите функцию, которая принимает название товара, его цену и категорию, добавляет этот товар в 
// массив, где каждый товар это объект и возвращает массив всех товаров. Товаров может быть сколько угодно.
// [
// {
// name: 'pen',
// price: 2,
// category: pens
// }
// ]
// - Напишите функцию, которая фильтрует товары по цене от и до и возращает новый массив только с товарами 
// выбранного ценового диапазона или пустой массив.+
// - Напишите функцию, которая фильтрует товары по категории и возращает новый массив только с товарами
//  выбранной категории, если она есть или пустой массив.+
// - Напишите функцию, которая возвращает количесто товаров в категории.+
// - Напишите функцию, которая удаляет товар по имени. +
// - Напишите функции, которые сортируют товары по цене от меньшего к большему и наоборот и 
// возвращают новый массив. +
// - Напишите функцию, котрая принимает вид сортировки (от большего к меньшему или наоборот) и 
// фильтра (диапазон цены или категория) и возвращает новый массив товаров определённой выборки, 
// отсортированные как указал пользователь. +
// - Напишите функцию, котрая принимает фильтра (диапазон цены или категория) и возвращает сумму цен
//  товаров этой выборки. +
// В этом задании переиспользуйте свои функции, где можно, что бы не дублировать функционал.
// В итоге, пользователь может добавлять, удалять, сортировать, считать количество, фильтровать и считать 
// сумму товаров.



var gedCart = function () {
	// Общий массив - карзина и фильтрованый массив
	var cart = [];
	var modifiedCart = [];

	// Параметры фильтрации и сортировки
	var parametrs = {
		sort: "none",
		filter: "none"
	};

	// Приватные методы
	// Функция заполнения корзины
	function fill() {
		var userName = prompt("Введите наименование товара");
		if(userName === null) return;
		var userPrice = prompt("Введите цену продукта");
		userPrice = Number(userPrice).toFixed(2);
		if(userPrice === null) return;
		var userCategory = prompt("Введите категорию товара");
		if(userCategory === null) return;
		setCart (userName, userPrice, userCategory);
		repeat(fill);
	}
	// Функция структурирования объекта-товара, принимает имя, цену и категорию,
	// возвращает массив с товарами
	function setCart(name, price, category) {
		var product = {};
		var massage = false;
		isFullParam(name) ? product.name = name: massage = "Название некорректно";
		isValidPrice(price) ? product.price = price: massage = "Проверьте ввод цены";
		isFullParam(category) ? product.category = category: massage = "Проверьте ввод категории";
		if(massage) {
			alert(massage);
			return;
		}
		cart.push(product);
		return cart;
	}
	// Функция проверки корректности цены, принимает цену, возвращает булевое значение
	function isValidPrice(price) {
		return isNumber(price) && price>0;
	}
	// Проверка имени и категории принимает параметр, возвращает true, если поле ввода заполнено
	function isFullParam(param) {
		return !!param;
	}
	// Функция повтора через подтверждение, принимает повторяемую функцию и повторяет, если положительный ответ пользователя
	function repeat(repeaterCb) {
		var repeater = confirm("Продолжить ввод");
		if(repeater) {
			repeaterCb();
		}
		return;
	}
	// Фильтр по цене, возвращает новый массив, отфильтрованый в заданом диапозоне
	function filterByPrice() {
		var minValue = +prompt("Введите минимальную цену");
		var maxValue = +prompt("Введите максимальную цену");
		modifiedCart = cart.filter(
			function (item) {
				return item.price >= minValue && item.price <= maxValue;
			});
		return modifiedCart;
	}
	// Фильтр по категории товара, возвращает новый массив, отфильтрованый по категории
	function filterByCategory() {
		var category = prompt("Введите категорию товара");
		modifiedCart = cart.filter(
			function (item) {
				return item.category.toLowerCase() === category.toLowerCase();
			});
		return modifiedCart;
	}
	// Функция возвращает количество товаров в ведённой категории
	function getCategoryQuantity() {
		filterByCategory();
		return modifiedCart.length;
	}
	// Функция удаления товара по его наименованию. Удаляет товар из основного массива и возвращает основной
	// массив без удалённого товара
	function nameRemove() {
		var removeName = prompt("Введите название удаляемого товара");
		cart = cart.filter(
			function (item) {
				return item.name.toLowerCase() !== removeName.toLowerCase();
			});
		return cart;
	}
	// Сортирует массив от меньшего к большему, возвращает сортированный массив
	function toMost() {
		modifiedCart = cart.sort(function(a, b) {
			return a.price - b.price;
		});
		return modifiedCart;
	}
	// Сортирует массив от большего к меньшему, возвращает сортированный массив
	function toLess() {
		modifiedCart = cart.sort(function(a, b) {
			return b.price - a.price;
		});
		return modifiedCart;
	}
	// Функция принимает вид сортирока и фильтрации (из ибъекта параметров) и возвращает сортированный массив
	function setFilter(sort, filter) {
		(typeof sort === "undefined")? sort = parametrs.sort: sort = sort;
		(typeof filter === "undefined")? filter = parametrs.filter: filter = filter;		
		(typeof sort === "function")? sort(): alert("Выберете тип сортировки");
		(typeof filter === "function")? filter(): alert("Выберете тип фильтра");
		return modifiedCart;
	}
	// Функция принимает вид фильтрации, возврацает сумму цен отфильтрованного массива
	function setPriceSum(filter) {
		var arr = setFilter(toLess, filter);
		var calculatedPrice = 0;
		for(var i = 0; i < arr.length; i++) {
			calculatedPrice += Number(arr[i].price);
		}
		return calculatedPrice.toFixed(2);
	}
	// Функция очистки корзины, возвращает пустой основной массив
	function clearCart() {
		cart = [];
		return cart;
	}
	// Функция отрисовки корзины. Принимает массив и отрисовывает его на странице
	function renderCart(target) {
		var cartOutput = document.querySelector(".cart-output");
		var cartList = document.createElement("ul");
		var cartItems = [];
		var itemTitles = [];
		var itemPrices = [];
		var itemCategory = [];
		cartList.classList.add("cart__list");
		if(cartOutput.childNodes.length) {
			cartOutput.removeChild(cartOutput.firstChild);
		}
		if(target.length === 0) {
			cartList = document.createElement("p");
			cartList.textContent = "Нам нечего Вам показать, корзина пуста =(";
		}
		cartOutput.appendChild(cartList);
		for(var i = 0; i < target.length; i++) {
			cartItems[i] = document.createElement("li");
			cartItems[i].classList.add("cart__item");
			itemTitles[i] = document.createElement("h3");
			itemTitles[i].classList.add("cart__title");
			itemTitles[i].textContent = `Название: ${target[i].name}`;
			itemPrices[i] = document.createElement("p");
			itemPrices[i].classList.add("cart__price");
			itemPrices[i].textContent = `Цена: ${target[i].price} у.е.`;
			itemCategory[i] = document.createElement("p");
			itemCategory[i].classList.add("cart__category");
			itemCategory[i].textContent = `Категория: ${target[i].category}`;
			cartList.appendChild(cartItems[i]);
			cartItems[i].appendChild(itemTitles[i]);
			cartItems[i].appendChild(itemPrices[i]);
			cartItems[i].appendChild(itemCategory[i]);
		}
	}

	// Публичные методы состаят из вызова рабочей функции, параметров фильтрации/сортировки, настройки этих
	// параметров, вывода сообщений и функции отрисовки
	return {
		// Параметры сортировки
		parametrs: parametrs,
		fillCart: function() {
			fill();
			renderCart(cart);
		},
		priceFilterCart: function() {
			parametrs.filter = filterByPrice;
		},
		categoryFilterCart: function() {
			parametrs.filter = filterByCategory;
		},
		categoryFiltredQuantity: function() {
			alert(`В введённой Вами категории - ${getCategoryQuantity()} товаров`);
		},
		removeByName: function() {
			nameRemove();
			renderCart(cart);
		},
		sortToMost: function() {
			parametrs.sort = toMost;
		},
		sortToLess: function() {
			parametrs.sort = toLess;
		},
		filter: function() {
			setFilter();
			renderCart(modifiedCart);
		},
		getPriceSum: function() {
			alert(`Сумма отфильтрованых товаров - ${setPriceSum()} у.е.`);	
		},
		getClear: function() {
			clearCart();
			renderCart(cart);
		}
	};
};

