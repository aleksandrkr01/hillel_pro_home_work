var levelSettings = [
  {
    timeMultiplayer: 1.00,
    pointsIndex: 1.00,
    pointsLimit: 200
  },
  {
    timeMultiplayer: 0.98,
    pointsIndex: 1.10,
    pointsLimit: 500
  },
  {
    timeMultiplayer: 0.94,
    pointsIndex: 1.15,
    pointsLimit: 700
  },
  {
    timeMultiplayer: 0.88,
    pointsIndex: 1.20,
    pointsLimit: 900
  },
  {
    timeMultiplayer: 0.80,
    pointsIndex: 1.25,
    pointsLimit: 1200
  },
  {
    timeMultiplayer: 0.76,
    pointsIndex: 1.30,
    pointsLimit: 1500
  },
  {
    timeMultiplayer: 0.70,
    pointsIndex: 1.35,
    pointsLimit: 1700
  },
  {
    timeMultiplayer: 0.65,
    pointsIndex: 1.40,
    pointsLimit: 1800
  },
  {
    timeMultiplayer: 0.60,
    pointsIndex: 1.45,
    pointsLimit: 1900
  },
  {
    timeMultiplayer: 0.5,
    pointsIndex: 1.50,
    pointsLimit: 2000
  }
];