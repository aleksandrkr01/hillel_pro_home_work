var fruits = [
  {
  	'name': 'orange',
  	'points': 1.0,
  	'src': 'src/003-orange.svg',
  	'speed': 1.0,
  	'gotFruit': 0
  },
  {
  	'name': 'apple',
  	'points': 1.1,
  	'src': 'src/001-apple.svg',
  	'speed': 1.1,
  	'gotFruit': 0
  },
  {
  	'name': 'strawberry',
  	'points': 1.2,
  	'src': 'src/002-strawberry.svg',
  	'speed': 1.2,
  	'gotFruit': 0
  },
  {
  	'name': 'tomato',
  	'points': 1.3,
  	'src': 'src/004-tomato.svg',
  	'speed': 1.3,
  	'gotFruit': 0
  },
  {
  	'name': 'grapes',
  	'points': 1.4,
  	'src': 'src/005-grapes.svg',
  	'speed': 1.4,
  	'gotFruit': 0
  },
  {
  	'name': 'watermelon',
  	'points': 1.5,
  	'src': 'src/006-watermelon.svg',
  	'speed': 1.5,
  	'gotFruit': 0
  },
  {
  	'name': 'pineapple',
  	'points': 1.6,
  	'src': 'src/007-pineapple.svg',
  	'speed': 1.6,
  	'gotFruit': 0
  },
  {
  	'name': 'cherries',
  	'points': 1.7,
  	'src': 'src/008-cherries.svg',
  	'speed': 1.7,
  	'gotFruit': 0
  },
  {
  	'name': 'lemon',
  	'points': 1.8,
  	'src': 'src/009-lemon.svg',
  	'speed': 1.8,
  	'gotFruit': 0
  },
  {
  	'name': 'raspberry',
  	'points': 1.9,
  	'src': 'src/010-raspberry.svg',
  	'speed': 1.9,
  	'gotFruit': 0
  },
  {
  	'name': 'chili',
  	'points': 2.0,
  	'src': 'src/011-chili.svg',
  	'speed': 2.0,
  	'gotFruit': 0
  },
  {
  	'name': 'carrot',
  	'points': 2.1,
  	'src': 'src/012-carrot.svg',
  	'speed': 2.1,
  	'gotFruit': 0
  },
  {
  	'name': 'salad',
  	'points': 2.2,
  	'src': 'src/013-salad.svg',
  	'speed': 2.2,
  	'gotFruit': 0
  },
  {
  	'name': 'steak',
  	'points': 2.3,
  	'src': 'src/014-steak.svg',
  	'speed': 2.3,
  	'gotFruit': 0
  }
];




