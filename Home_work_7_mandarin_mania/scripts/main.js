/*Далее предлагаю вам варианты доработки игры:
1. Добавить возможность рестарта до перезагрузки страницы.+
2. Добавить возможность выбрать сложность.+
3. Добавить возможность выбрать фрукт.+
4. Изменять фрукт при клике.+
5. Сохранять результаты (до перезагрузки страницы), отображать их пользователю и сортировать от больших к меньшим (Highest scores).+
6. * Сохранять результаты в Local Storage - https://tproger.ru/articles/localstorage/+
7. * Добавить выбор количества фруктов с разной стоимостью. 
Вывести пользователю легенду выбранной комбинации. 
Считать очки по каждому фрукту и сумму. +
8. * Добавить уровни (усложнение при достижении определённых результатов).+*/
'use strict';

// Заранее извеняюсь за то, что код похож на код лосося-курильщика под тяжлыми наркотиками, и количество обращений к
// какой-то абстрактной матери привышает допустимую норму, и вообще: "Ольга Фреймут не рекомендує даний код"

alert('WELCOME TO MANDARIN MANIA!');

var convas = document.querySelector('.game-canvas');
var fruit = document.createElement('div');
var timeText = document.getElementById('timeText');
var shift = 90;
var fruitTimer;
var gameTime = 30;
var startTime = new Date().getTime();
var startTimeStep = 1000;
var startButton = document.querySelector('.btn-start');
var restartButton = document.querySelector('.btn-restart');
var timeStep;
var timer;
var startCode = 83;
var restartCode = 82;
var settings = document.querySelector('.game-settings');
var fruitSelect = document.querySelector('#fruit-select');
var index = +fruitSelect.value;
var amount = 0;
var points = 0;


// Настройки для уровня игры

var levels = document.querySelector('.levels');
var level;
var isFixedLevel = document.querySelector('.level-checbox');

function setupLevel() {
  level = levels.value-1;
};

function setIncrease() {
  if(points>=levelSettings[level].pointsLimit && level<levelSettings.length-1) {
    ++levels.value;
    ++level;
    timeStep = startTimeStep*(1/fruits[index].speed)*levelSettings[level].timeMultiplayer;
  };
};

function setAnnul() {
  levels.value = 1;
  level = levels.value-1;
};

function increaseLevel() {
  if(!isFixedLevel.checked) {
    setIncrease();
  };
};

function annulLevel() {
  if(!isFixedLevel.checked) {
    setAnnul();
  };
};

// Настройки игры

for(var i = 0; i < fruits.length; i++) {
  fruitSelect.add(new Option(fruits[i].name, i));
};
fruitSelect.style.backgroundImage = 'url(' + fruits[index].src + ')';
takeResult(results);
renderResult(results, resultDictionay);

// Главная функция, запускающая игру

function startGame(request) {
  if(request || request === undefined) {
    clearInterval(timer);
    clearInterval(fruitTimer);
    resetBlock();
    if(!isFixedLevel.checked) {
      annulLevel();
    }
    startCombine();
    setupLevel();
    startButton.textContent = 'Restart (R)';
    setTimer();
    setPageTimer();
    timer = setInterval(setPageTimer, startTimeStep);
    renderFruit(fruit); 
    document.removeEventListener('keyup', presserS);
    document.addEventListener('keyup', presserR);
  }
  return;
};

// Обнуление игры

function resetBlock() {
  startTime = new Date().getTime();
  points = 0;
  amount = 0;
  resultText.textContent = 'Result'; 
};

// Настройка таймера игры

function setTimer() {
  (mode === 'multifruit' || mode === 'combine')? timeStep = startTimeStep*levelSettings[level].timeMultiplayer:
  timeStep = startTimeStep*(1/fruits[index].speed)*levelSettings[level].timeMultiplayer;
  fruitTimer = setInterval(function() {
    moveFruit(fruit);
  }, timeStep);
};

// Настройка таймера страницы

function setPageTimer() {
  var currentTime = new Date().getTime();
  var difference = currentTime - startTime;
  var seconds = Math.round(difference / 1000);
  if (seconds >= gameTime) {
    clearInterval(timer);
    clearInterval(fruitTimer);
    timeText.textContent = 'Time left';
    fruit.style.display = 'none';
    btnsUnblock();
    setResult(results);
  } else {
    btnsBlock();
    timeText.textContent = gameTime - seconds;
  };
};

// Блокировка-разблокировка кнопок во время игры

function btnsBlock() {
  settings.setAttribute('disabled', true);
};

function btnsUnblock() {
  settings.removeAttribute('disabled');
};

// Отрисовка фрукта

function renderFruit(fruit) {
  fruit.style.display = 'block';
  fruit.style.backgroundImage = `url(${fruits[index].src})`;
  if(mode !== 'combine') {
    fruitSelect.style.backgroundImage = 'url(' + fruits[index].src + ')';
  };
  fruit.classList.add('fruit');
  convas.appendChild(fruit);
  moveFruit(fruit);
};

// Передвижение фрукта

function moveFruit(fruit) {
  fruit.style.top = Math.round(Math.random() * shift) + '%';
  fruit.style.left = Math.round(Math.random() * shift) + '%';
};

// Поведение фрукта при клике

fruit.addEventListener('click', function(evt) {
  clearInterval(fruitTimer);
  moveFruit(fruit);
  getResult();
  setTimer();
  if(mode === 'multifruit' || mode === 'combine') {
    increaseValue();
    changeFruit();
    renderConbin();
  };
  if(!isFixedLevel.checked) {
    increaseLevel();
  };
});

// Функция изменения фрукта

function changeFruit() {
  index = Math.floor(Math.random()*fruits.length);
  fruit.setAttribute('data-value', fruits[index].name);
  timeStep = startTimeStep*levelSettings[level].timeMultiplayer;
  renderFruit(fruit);
};

// Функции отработки старта/рестарта при нажатии на кнопки

function presserS(key) {
  if(key.keyCode === startCode) {
    startGame(confirm('Are you ready?'));
  };
};

function presserR(key) {
  if(key.keyCode === restartCode) {
    startGame(confirm('Are you ready?'));
  };
};

// Отработка нажатия на старт

startButton.addEventListener('click', function() {
  startGame(confirm('Are you ready?'));
});

document.addEventListener('keyup', presserS);

// Определение фрукта при выботе из селекта

fruitSelect.addEventListener("change", function() {
  index = +fruitSelect.value;
  if(mode !== 'combine') {
    fruitSelect.style.backgroundImage = 'url(' + fruits[index].src + ')';
  };
});

