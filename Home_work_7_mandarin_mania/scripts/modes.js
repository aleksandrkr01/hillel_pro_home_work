'use strict';


// Настройки режимов игры


var mode;
var fruitSelect = document.querySelector('#fruit-select');
var isMultifruit = document.querySelector('.multifruit-checbox');
isMultifruit.addEventListener('change', setMultifruit);
var isCombination = document.querySelector('.combination-checbox');
isCombination.addEventListener('change', setCombine);
var combineItems = fruitSelect.selectedOptions;
var selectedFruits = [];
var combineBlock = document.querySelector('.conbine-block');
var selectedFruits;
var swap = fruits;

function setMultifruit() {
  if(isMultifruit.checked) {
    mode = 'multifruit';
    fruitSelect.value = 'multifruit';
    fruitSelect.setAttribute('disabled', true);
  }
  else {
    mode = '';
    fruitSelect.removeAttribute('disabled');
    fruitSelect.value = index;
  };
};

function startCombine() {
  if(mode === 'combine') {
    annulCombin();
    initCombine();
  };
};

function setCombine() {
  combineBlock.hidden = false;
  if(isCombination.checked) {
    combineBlock.classList.remove('hidden');
    mode = 'combine';
    fruitSelect.value = index;
    isMultifruit.checked = false; 
    isMultifruit.setAttribute('disabled', true);
    fruitSelect.setAttribute('multiple', true);
    fruitSelect.removeAttribute('disabled');
    fruitSelect.style.backgroundImage = 'none';
    fruitSelect.style.height = '220px';
  }
  else {
    combineBlock.classList.add('hidden');
    mode = '';
    fruitSelect.removeAttribute('multiple');
    isMultifruit.removeAttribute('disabled');
    fruits = swap;
    index = 0;
    fruitSelect.value = index;
    fruitSelect.style.backgroundImage = 'url(' + fruits[index].src + ')';
    fruitSelect.style.height = 'auto';
  };
};

function initCombine() {
  getCombine();
  fruits = selectedFruits;
  index = 0;
  fruit.setAttribute('data-value', fruits[index].name); 
  renderConbin();
};

function getCombine() {
  let filterIndex;
  for (let i = 0; i < combineItems.length; i++) {
    filterIndex = combineItems[i].value;
    selectedFruits.push(fruits[filterIndex]);
  };
};

function increaseValue() {
  var clickedFruit = fruit.getAttribute('data-value')
  for(let i = 0; i < selectedFruits.length; i++) {
    if(clickedFruit == selectedFruits[i].name) {
      selectedFruits[i].gotFruit++
    };
  };
};

function annulCombin() {
  fruit.removeAttribute('data-value');
  selectedFruits.forEach((item) => {
    item.gotFruit = 0;
  });
  selectedFruits = [];
  fruits = swap;
}

function renderConbin() {
  var container = document.createElement('div');
  var list = document.createElement('ul');
  list.classList.add('combine-list');
  var isFullBlock = combineBlock.children[0];
  var listItem;  
  var itemContent;
  var itemCount;
  var total = document.createElement('p');
  total.classList.add('combine-total');
  total.textContent = `Total: ${batchTotals()}`;
  for(var i = 0; i < selectedFruits.length; i++) {
    listItem = document.createElement('li');
    listItem.classList.add('combine-item');
    listItem.style.listStyleImage = 'url(' + selectedFruits[i].src + ')';
    list.appendChild(listItem);
    itemContent = document.createElement('p');
    itemContent.classList.add('combine-description');
    itemContent.textContent = selectedFruits[i].name;
    listItem.appendChild(itemContent);
    itemCount = document.createElement('span');
    itemCount.textContent = selectedFruits[i].gotFruit;
    listItem.appendChild(itemCount);
  };
  container.appendChild(list);
  container.appendChild(total);
  if(isFullBlock) {
    combineBlock.removeChild(isFullBlock);
  };
  combineBlock.appendChild(container);
};

function batchTotals() {
  var total = 0;
  for(var i = 0; i < selectedFruits.length; i++) {
    total += selectedFruits[i].gotFruit;
  };
  return total;
};
