'use strict';


// Настройки сбора, подсчёта и отражения результатов


var results = [];
var resultsLength = 10;
var resultText = document.getElementById('resultText');

// Просто словарь классов для отрисовки результата

var resultDictionay = {
	blockClass: 'results-block',
	tableClass: 'result-table',
	headerClass: 'result-header',
	bodyClass: 'result-body',
	headerDataClass: 'result-header-data',
	resultDataClass: 'result-data',
	showLimit: resultsLength,
};

function setResult(array) {
  takeResult(array);
  fillResult(array);
  sortResult(array);
  renderResult(array, resultDictionay);
  saveResult(array);
};

function fillResult(array) {
  var resObj = {};
  resObj.result = points;
  resObj.level = level+1;
  array.push(resObj);
};

function sortResult(array) {
  function compareResult(a, b) {
    return b.result - a.result;
  };
  array = array.sort(compareResult);
};

function renderResult(array, dictionary) {
  var resultBlock = document.querySelector(`.${dictionary.blockClass}`);
  var table = document.createElement('table');
  table.classList.add(`${dictionary.tableClass}`);
  var tableHeader = document.createElement('thead');
  tableHeader.classList.add(`${dictionary.headerClass}`);
  var tableBody = document.createElement('tbody');
  tableBody.classList.add(`${dictionary.bodyClass}`);
  var tableFill = resultBlock.children[1];
  var tableHeaderData = [];
  var resultRow = [];
  var resultData = [];
  var index = 0;
  table.appendChild(tableHeader);
  table.appendChild(tableBody);
  for(var key in array[index]) {
    tableHeaderData[key] = document.createElement('th');
    tableHeaderData[key].textContent = key;
    tableHeaderData[key].classList.add(`${dictionary.headerDataClass}`);
    tableHeader.appendChild(tableHeaderData[key]);
  };
  for (index; index < dictionary.showLimit; index++) {
    resultRow[index] = document.createElement('tr');
    tableBody.appendChild(resultRow[index]);
    for(var key in array[index]) {
      resultData[key] = document.createElement('td');
      resultData[key].classList.add(`${dictionary.resultDataClass}`);
      resultData[key].textContent = array[index][key];
      resultRow[index].appendChild(resultData[key]);
    };
  };
  if(tableFill) {
    resultBlock.removeChild(tableFill);
  };
  resultBlock.appendChild(table);
};

function takeResult(array) {
  var startResults = JSON.parse(localStorage.getItem('result'));
  if(startResults) {
      startResults.forEach(function(item, index) {
      array[index] = item;
    });
  };
  return;
};

function saveResult(array) {
  var resultString = JSON.stringify(array);
  localStorage.setItem('result', resultString);
};

function getResult() {
  ++amount;
  points += (Math.round(fruits[index].points*levelSettings[level].pointsIndex*1000))/10;
  resultText.textContent = 'You collected ' + amount + ' fruits. Your points: ' + points;
};
