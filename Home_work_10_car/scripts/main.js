
// Вам необходимо выполнить следующие пункты:
// 1. Добавьте возможность указывать расход масла автомобиля, при его создании.+
// 2. Добавьте возможность указания максимальной скорости при создании, а так же скорости поездки при поездке. 
// На основе этих данных вычисляйте затраченное на поездку время.+
// 3. ** Укажите пропорционально в константах расход топлива при разной скорости и вычисляйте расход на указанную для 
// поездки скорость (сложное, не обязательно к выполнению. Кто осилит.).+

// В приложении пример с работой программы в консоле. Создайте свой автопарк.

let cars = [];
let carBtn = document.querySelector('.constructor__greate-car');
let form = document.querySelector('.constructor__form');
let car;
let carsOutput = document.querySelector('.cars__output');

function createCar(evt) {
  evt.preventDefault();
  let carNameInp = document.querySelector('.constructor__name');
  let gasConsumptionInp = document.querySelector('.constructor__gas-consumption');
  let oilConsumptionInp = document.querySelector('.constructor__oil-consumption');
  let maxSpeedInp = document.querySelector('.constructor__max-speed');
  let carName = carNameInp.value;
  let gasConsumption = Number(gasConsumptionInp.value);
  let oilConsumption = Number(oilConsumptionInp.value)
  let maxSpeed = Number(maxSpeedInp.value)
  if (isValidParametr(gasConsumption) && isValidParametr(oilConsumption) && isValidParametr(maxSpeed)) {
    car = new Car(gasConsumption, oilConsumption, maxSpeed, carName);
    cars.push(car);
  }
  else return;
  car.__renderCars(cars);
  //не знаю насколько хорош такой reset в этом контексте
  form.reset();
  carNameInp.focus();
};

function changeImg(outerEvt) {
  var input = document.createElement('input');
  input.type = 'file';
  input.accept = 'image/jpeg, image/png, image/bmp';
  input.click();
  input.addEventListener('change', function(innerEvt) {
    var file = innerEvt.target.files[0]; 
    if(file.type.match(/image.*/)) {
      outerEvt.target.setAttribute('src', URL.createObjectURL(file));
    }
    else alert('Wrong file type');
  });
};

/**
 * Check value to number.
 * @param n {void} - value.
 * @return {boolean} - Is number.
 */
function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
};

function isValidParametr(parametr) {
  return (isNumeric(parametr) && parametr > 0);
};

carBtn.addEventListener('click', createCar);


/**
 * For starting - create car with gas consumption, oil consumption, max speed:
 * var car = Car(5, 0.1, 200);
 *
 * Car module. Created car with custom gas consumption, oil consumption, max speed.
 * @param gasConsumption {number}
 * @param oilConsumption {number}
 * @param maxSpeed {number}
 * @return {{
 *            start: start,
 *            riding: riding(speed),
 *            checkGas: checkGas,
 *            checkOil: checkOil,
 *            gasRefueling: gasRefueling,
 *            oilRefueling: oilRefueling
 *          }}
 * @constructor
 */
function Car(gasConsumption, oilConsumption, maxSpeed, name) {
  
  if (!isNumeric(gasConsumption) || gasConsumption <= 0) {
    showMessage('Wrong gas consumption', 'error');
    return;
  }
  if (!isNumeric(oilConsumption) || oilConsumption <= 0) {
    showMessage('Wrong oil consumption', 'error');
    return;
  }
  if (!isNumeric(maxSpeed) || maxSpeed <= 0) {
    showMessage('Wrong max speed', 'error');
    return;
  }

  let gasBalance = 100;
  let oilBalance = 100;
  let currGasConsumption;
  let currOilConsumption = oilConsumption/100;
  let gasResidue;
  let oilResidue;
  let gasVolume = 200;
  let oilVolume = 200;
  let ignition = false;
  let ready = false;
  let self = this;
  let dataInput;
  let speedInput;
  let messageOutput;
  let carBtnBlock;
  let controls = [
    {
      name: 'Старт',
      addClass: 'btn__start',
      handler: 'start'
    },
    {
      name: 'Ехать',
      addClass: 'btn__ride',
      handler: 'riding',
      isValue: true,
      label: 'Расстояние',
    },
    {
      name: 'Проверить бензин',
      addClass: 'btn__gas-check',
      handler: 'checkGas'
    },
    {
      name: 'Проверить масло',
      addClass: 'btn__oil-check',
      handler: 'checkOil'
    },
    {
      name: 'Заправить бензин',
      addClass: 'btn__gas-refuil',
      handler: 'gasRefueling',
      isValue: true,
      label: 'Объём бензина',
    },
    {
      name: 'Залить масло',
      addClass: 'btn__oil-refuil',
      handler: 'oilRefueling',
      isValue: true,
      label: 'Объём масла',
    },
  ];

  /**
   * Renders cars array on the screen.
   * @param array {Array} - Cars array.
   */

  this.__renderCars = function(array) {
    for(let i = 0; i < cars.length; i++) {
      var carBlock = document.createElement('div');
      carBlock.classList.add('cars__card');
      var carName = document.createElement('h3');
      carName.classList.add('cars__title');
      let carImg = document.createElement('img');
      carImg.classList.add('cars__image');
      carImg.setAttribute('src', 'img/car-mock.jpg');
      carImg.setAttribute('title', 'Change image');
      carImg.addEventListener('click', changeImg);
      var carInfo = document.createElement('p');
      carInfo.classList.add('car-info');
      carInfo.textContent = `Name: ${self.name}. Gas consumption: ${gasConsumption}. Oil consumption: ${oilConsumption}. Max speed: ${maxSpeed}. `
      carBtnBlock = document.createElement('form');
      carBtnBlock.classList.add('cars__btns');
      carBlock.appendChild(carName);
      carName.textContent = self.name;
      messageOutput = document.createElement('p');
      messageOutput.classList.add('output-massage');
      messageOutput.textContent = 'Вывод сообщения';
      carBlock.appendChild(carImg);
      carBlock.appendChild(carInfo);
      carBlock.appendChild(carBtnBlock);
      carBlock.appendChild(messageOutput);
    }
    for(let i = 0; i < controls.length; i++) {
      let button = document.createElement('button');
      button.classList.add(controls[i].addClass);
      button.textContent = controls[i].name;
      button.setAttribute('data-action', controls[i].handler);
      carBtnBlock.appendChild(button);
    }
    setParametrs(carBtnBlock);
    carsOutput.appendChild(carBlock);
    carBlock.addEventListener('click', carWork);
  };

  /**
   * Renders cars array on the screen.
   * @param array {Array} - Cars array.
   */

  function setParametrs(parrentBlock) {
    dataInput = document.createElement('input');
    dataInput.setAttribute('type', 'number');
    dataInput.classList.add('input--wide');
    var label = document.createElement('label');
    label.textContent = 'Расстояние/объём бензина, объём масла';
    speedInput = document.createElement('input');
    speedInput.setAttribute('type', 'number');
    speedInput.classList.add('input--wide');
    var speedLabel = document.createElement('label');
    speedLabel.textContent = 'Скорость езды';
    for(let i = 0; i < cars.length; i++) {
      for(let j = 0; j < cars.length; j++) {
        label.setAttribute('for', 'dataInput' + j);
        dataInput.id = 'dataInput' + j;
        dataInput.name = 'dataInput' + j;
        speedLabel.setAttribute('for', 'speedInput' + j + i);
        speedInput.id = 'speedInput' + j + i;
        
      };
    };
    parrentBlock.appendChild(dataInput);
    parrentBlock.insertBefore(label, dataInput);
    parrentBlock.appendChild(speedInput);
    parrentBlock.insertBefore(speedLabel, speedInput);
  };

  /**
   * Defines handler on car's button.
   * @param evt {event object} - User's event object.
   */

  function carWork(evt) {
    var target = evt.target;
    var curTarget = evt.currentTarget;
    if (target.tagName === 'BUTTON') {
      evt.preventDefault();
      var action = target.getAttribute('data-action');
      let dataValue = Number(dataInput.value);
      let speedValue = Number(speedInput.value);
      self[action](dataValue, speedValue);
      carBtnBlock.reset();
      dataInput.focus();
    }
    else return;
  };

  /**
   * Defines type of message output.
   * @param text {text} - Message text.
   */
  function setSuccess(text) {
    messageOutput.classList = '';
    messageOutput.classList.add('output-massage', 'output-massage--success')
    messageOutput.textContent = text;
  };

  function setError(text) {
    messageOutput.classList = '';
    messageOutput.classList.add('output-massage', 'output-massage--error')
    messageOutput.textContent = text;
  };

  function setWarning(text) {
    messageOutput.classList = '';
    messageOutput.classList.add('output-massage', 'output-massage--warning')
    messageOutput.textContent = text;
  };

  /**
	 * Check gas amount after riding.
	 * @param distance {number} - Riding distance.
	 * @return {number} - Gas amount.
	 */
  function gasCheck(distance) {
    if (gasBalance <= 0) {
      return 0;
    }

    let gasForRide = Number((distance * currGasConsumption).toFixed(2));

    return gasBalance - gasForRide;
  };

  /**
   * Check oil amount after riding.
   * @param distance {number} - Riding distance.
   * @return {number} - Oil amount.
   */

  function oilCheck(distance) {
    if (oilBalance <= 0) {
      return 0;
    }

    let oilForRide = (distance * currOilConsumption).toFixed(2);

    return oilBalance - oilForRide;
  };

  /**
	 * Show message for a user in the console.
	 * @param message {string} - Text message for user.
	 * @param type {string} - Type of the message (error, warning, log). log by default.
	 */

  function showMessage(message, type) {
    let messageText = message ? message : 'Error in program. Please call - 066 083 07 06';

    switch (type) {
      case 'error':
        setError(messageText);
        break;
      case 'warning':
        setWarning(messageText);
        break;
      case 'log':
        setSuccess(messageText);
        break;
      default:
        setSuccess(messageText);
        break;
    }
  };

  /**
	 * Check car for ride.
	 * @param distance {number} - Ride distance.
	 */

  function checkRide(distance) {
    gasResidue = Number(gasCheck(distance).toFixed(2));
    oilResidue = Number(oilCheck(distance).toFixed(2));
  };

  /**
   * Check car's speed.
   * @param speed {number} - Ride speed.
   */

  function checkSpeed(speed) {
    if(!isNumeric(speed) || speed <= 0) {
      showMessage('Wrong speed', 'error');
      return false;
    }
    else if(speed > maxSpeed) {
      showMessage('You can\'t ride so fast', 'error');
      return false;
    }
    return true;
  };

  /**
   * Sets car's consumption.
   * @param speed {number} - Ride speed.
   */

  function setConsumption(speed) {
    const CONSUMPTION_COEF_STANDART = 1;
    const CONSUMPTION_COEF_MEDIUM = 2;
    const CONSUMPTION_COEF_FAST = 3;
    const CONSUMPTION_COEF_EXTRA_FAST = 3.5;
    const STABLE_SPEED = 60;
    const FAST_SPEED = 80;
    const EXTRA_SPEED = 100;
    switch (true) {
      case (speed<=STABLE_SPEED):
        currGasConsumption = (gasConsumption / 100) * CONSUMPTION_COEF_STANDART;
        break;
      case (speed<=FAST_SPEED):
        currGasConsumption = (gasConsumption / 100) * CONSUMPTION_COEF_MEDIUM;
        break;
      case (speed<=EXTRA_SPEED): 
        currGasConsumption = (gasConsumption / 100) * CONSUMPTION_COEF_FAST;
        break;
      case (speed>EXTRA_SPEED):
        currGasConsumption = (gasConsumption / 100) * CONSUMPTION_COEF_EXTRA_FAST;
        break;
    }
    return currGasConsumption.toFixed(2);
  };

  /**
   * Public methods object.
   */

  /**
   * Car name.
   */

  this.name = name;

  /**
   * Start car.
   */

  this.start = function() {
    ignition = true;
    if (gasBalance <= 0) {
      showMessage('You don\'t have gas. Please refuel the car.', 'error');
      ready = false;
      return;
    }
    if (oilBalance <= 0) {
      showMessage('You don\'t have oil. Please pour oil into the car.', 'error');
      ready = false;
      return;
    }

    ready = true;

    showMessage('Ingition', 'log');
  };

  /**
   * Riding function.
   * @param distance {number} - Riding distance.
   * @param rideSpeed {number} - Riding speed.
   */

  this.riding = function(distance, rideSpeed) {
    
    if (!isNumeric(distance) || distance <= 0) {
      showMessage('Wrong distance', 'error');
      return;
    }      

    if (!ignition) {
      showMessage('You need start car', 'error');
      return;
    }

    if (!ready) {
      ignition = false;
      showMessage('You need gas station', 'error');
      return;
    }

    setConsumption(rideSpeed);
    checkRide(distance);
    
    if (!checkSpeed(rideSpeed)) {
      return;
    }

    let gasDriven = Math.round(gasBalance / currGasConsumption);
    let oilDriven = Math.round(oilBalance / currOilConsumption);
    var rideTime = (distance/rideSpeed).toFixed(2); 
    if (gasResidue <= 0 || oilResidue <= 0) {
      let distanceGasLeft = Math.round(distance - gasDriven);
      let distanceOilLeft = Math.round(distance - oilDriven);
      let neddedGas = (distanceGasLeft * currGasConsumption).toFixed(2);
      let neddedOli = (distanceOilLeft * currOilConsumption).toFixed(2);
     
      (gasResidue <= 0 && oilResidue <= 0) ? 
      (
        rideTime = (gasDriven/rideSpeed).toFixed(2),
        gasBalance = 0, 
        oilBalance = 0, 
        showMessage(`Gas and oil over. You have driven ${gasDriven} km. You need ${neddedGas} liters of gas and ${neddedOli} liters of oil. ${distanceGasLeft} km left. You was driven for ${rideTime} hours.`, 'warning')
      ): 
      (gasResidue <= 0) ? (
        rideTime = (gasDriven / rideSpeed).toFixed(2), 
        gasBalance = 0, 
        showMessage(`Gas over. You have driven - ${gasDriven}. You need ${neddedGas} liters. ${distanceGasLeft} km left. You was driven for ${rideTime} hours.`, 'warning')
      ): (
        rideTime = (oilDriven / rideSpeed).toFixed(2), 
        oilBalance = 0, 
        showMessage(`Oil over. You have driven - ${oilDriven}. You need ${neddedOli} liters of oil. ${distanceOilLeft} km left. You was driven for ${rideTime} hours.`, 'warning')
      );
    }
    if (gasResidue > 0 && oilResidue > 0) {
      gasBalance = gasResidue;
      oilBalance = oilResidue;
      showMessage(`You arrived. Gas balance - ${gasResidue}. Oli balance - ${oilResidue}. You spent ${rideTime} hours.`, 'log');
    }
  };

  /**
   * Check gas function.
   */

  this.checkGas = function() {
    showMessage('Gas - ' + gasBalance, 'log');
  };

  /**
   * Check oil function.
   */

  this.checkOil = function() {
    showMessage('Oil - ' + oilBalance, 'log');
  };

  /**
   * Gas refueling function.
   * @param gas
   */

  // Явно не нравится повторение функций, но из-за разницы в названиях переменных gas-- и oil--, проиходится делать так, дублируя код
  this.gasRefueling = function(gas) {
    if (!isNumeric(gas) || gas <= 0) {
      showMessage('Wrong gas amount', 'error');
      return;
    }

    if (gasVolume === gasBalance) {
      showMessage('Gasoline tank is full', 'warning');
    } else if (gasVolume < gasBalance + gas) {
      var currentGasBalance = gasBalance;
      gasBalance = gasVolume; 
      let excess = Math.round(gas - (gasVolume - currentGasBalance));
      showMessage('Gasoline tank is full. Excess - ' + excess, 'log');
    } else {
      gasBalance = Math.round(gasBalance + gas);
      showMessage('Gas balance - ' + gasBalance, 'log');
    }
  };

  /**
   * Oil refueling function.
   * @param oil
   */

  this.oilRefueling = function(oil) {
    if (!isNumeric(oil) || oil <= 0) {
      showMessage('Wrong oil amount', 'error');
      return;
    }

    if (oilVolume === oilBalance) {
      showMessage('Oil tank is full', 'warning');
    } else if (oilVolume < (oilBalance + oil)) {
      var currentOilBalance = oilBalance;
      oilBalance = oilVolume; 
      let excess = Math.round(oil - (oilVolume - currentOilBalance));
      showMessage('Oil tank is full. Excess - ' + excess, 'log');
    } else {
      oilBalance = Math.round(oilBalance + oil);
      showMessage('Oil balance - ' + oilBalance, 'log');
    }
  };
};
