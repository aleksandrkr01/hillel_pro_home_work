// Я прикрепляю для вас ссылку на проект "Users" - проект с урока. Приложение я не правил, как вы просили. В нём есть баги, 
// нужно разобраться почему так и исправить, а так же добавить новый функционал в существующий модуль:
// 1. Стилизация. Добавить "быструю" стилизацию при помощу Bootstrap 4 (https://getbootstrap.com/). 
// Привести в порядок форму и выводить пользователей в таблице.+
// 3. Возвращайте кнопке и полю изначальное состояние после редактирования.+
// 2. Добавить изначальные данные (массив с объектами - пользователями) и функцию init, которая выведет карточки с ними сразу.+
// 3. Следите за тем, что бы изначальный массив пользователей обновлялся по мере редактирования каждого пользователя.+
// 4. Разберитесь, почему редактирование более отного пользователя приводит к ошибке.+
'use strict';

let users = [
  {name: "fdf", id: "user656"}, 
  {name: "fdfdf", id: "user4565454"}, 
  {name: "fdf",}
];

let user;

let createButton = document.querySelector('#createButton');
let renameButton = document.querySelector('#renameButton'); 
let initButton = document.querySelector('#initButton');
let nameField = document.querySelector('#lastName');
let status = document.querySelector('#status');
let startMessage = document.querySelector('#cards__start-info');
let userForm = document.querySelector('.user-form');
const ENTER_CODE = 13;
const ESCAPE_CODE = 27;

createButton.classList.add('hidden');
nameField.classList.add('hidden');

function createUser(event) {
  event.preventDefault();
  let nameValue = nameField.value;

  if(nameValue.trim() === '') {
    defineStatus('User name can\'t be empty', 'status--error');
    return;
  };

  let resultName = defineId();
  user = new User(nameValue, resultName);
  user.addUser(nameValue, resultName);
  users.unshift(user);
  defineStatus('User successfully added', 'status--success');
  nameField.value = '';
}

function initUsers(event) {
  event.preventDefault();

  startMessage.remove();
  for(let i = 0; i < users.length; i++) {
    let nameValue = users[i].name;
    let resultName;

    (users[i].id)? resultName = users[i].id: resultName = defineId();
    
    user = new User(nameValue, resultName);
    user.addUser(nameValue, resultName);

    // Строка ниже перезаписывает пользователя из входящего массива на пользователя-экземпляр User
    users[i] = user;
    initButton.classList.add('hidden');
    createButton.classList.remove('hidden');
    nameField.classList.remove('hidden');
  };
}

function defineId() {
  let date = new Date;
  let hash = date.getTime();
  let resultName = 'user' + hash;
  return resultName;
}

function defineStatus(statusText, activeStatus) {
  clearClasses(status);
  status.textContent = statusText;
  status.classList.add('status', activeStatus);
  let timer = setTimeout(() => {
    status.classList.add('hidden');
  }, 2000);
}

function clearClasses() {
  for(let i = 0; i < arguments.length; i++) {
    arguments[i].classList = '';
  };
}

function keyControll(event, callBack) {
  switch(event.which) {
    case ENTER_CODE:
      callBack(event);
    break;
    case ESCAPE_CODE:
      nameField.value = '';
    break;
  }
}

function keyControllCreate(event) {
  keyControll(event, createUser)
}

initButton.addEventListener('click', initUsers);
createButton.addEventListener('click', createUser);

userForm.addEventListener('keydown', keyControllCreate);

function User(name, id) {
  let usersBlock = document.querySelector('#usersBlock');
  let card = document.createElement('div');
  let p = document.createElement('p');
  let avatar = document.createElement('img');
  let buttonsBlock = document.createElement('div');

  let self = this;

  let removeBtn = document.createElement('button');
  let userRenameBtn = document.createElement('button');

  let userName = '';
  let userId = null;

  let renameUser = function(id) {
    // Решил убрать инлайновые стили и менять классы
    createButton.classList.add('hidden');
    renameButton.classList.remove('hidden');
    nameField.focus();
    nameField.value = userName;
    renameButton.addEventListener('click', rename);
    userForm.addEventListener('keydown', keyControllRename);
    
    function keyControllRename(event) {
      keyControll(event, rename);
    }

    function rename(event) {
      event.preventDefault();
      renameButton.removeEventListener('click', rename);

      userName = nameField.value;
      p.textContent = nameField.value;
      renameButton.classList.add('hidden');
      createButton.classList.remove('hidden');
      
      nameField.value = '';
      userRenameBtn.disabled = false;
      
      userForm.removeEventListener('keydown', keyControllRename);
      userForm.addEventListener('keydown', keyControllCreate);

      self.name = userName;

      defineStatus('User successfully renamed', 'status--success');
    };
  }

  function createCard() {
    card.id = userId;
    
    p.textContent = userName;
    avatar.src = 'img/no_avatar.png';
    removeBtn.textContent = 'REMOVE';
    userRenameBtn.textContent = 'RENAME';

    card.appendChild(p);
    card.appendChild(avatar);
    card.appendChild(buttonsBlock);
    buttonsBlock.appendChild(removeBtn);
    buttonsBlock.appendChild(userRenameBtn);
    usersBlock.insertBefore(card, usersBlock.children[0]);

    let info = document.createElement('span');
    info.classList.add('card__info');
    info.textContent = 'Click image to change avatar';
    card.insertBefore(info, buttonsBlock);

    avatar.addEventListener('mouseover', function() {
      info.style.display = 'flex';
      avatar.addEventListener('mouseleave', function() {
        info.style.display = 'none';
      });
    });

    avatar.addEventListener('click', changeAvatar);

    usersBlock.classList.add('cards');
    card.classList.add('card');
    p.classList.add('card__name');
    avatar.classList.add('card__avatar');
    buttonsBlock.classList.add('card__buttons');
    removeBtn.classList.add('button', 'button--inside', 'removeBtn');
    userRenameBtn.classList.add('button', 'button--inside', 'userRenameBtn');

    let renameCallback = function(event) {
      event.preventDefault();
      userForm.removeEventListener('keydown', keyControllCreate);
      renameUser(userId);
      userRenameBtn.disabled = true;
    };

    userRenameBtn.addEventListener('click', renameCallback); 
  }

  let removeCallback = function(event) {
    event.preventDefault();
    this.parentNode.parentNode.remove();
    users.splice(users.indexOf(self), 1);
    defineStatus('User successfully removed', 'status--success');
    return users;
  }

  removeBtn.addEventListener('click', removeCallback);

  this.name = name;
  this.id = id;
  this.addUser = function(name, id) {
    userName = name;
    userId = id;
    createCard();
  }
};
