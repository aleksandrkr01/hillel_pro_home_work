// Похоже на мой первый сторонный модуль по смене аватарок

;var changeAvatar = function(outerEvt) {
  let input = document.createElement('input');
  input.type = 'file';
  input.accept = 'image/jpeg, image/png, image/bmp';
  input.click();
  input.addEventListener('change', function(innerEvt) {
    let file = innerEvt.target.files[0]; 
    if(file.type.match(/image.*/)) {
      outerEvt.target.setAttribute('src', URL.createObjectURL(file));
    }
    else alert('Wrong file type');
  });
};