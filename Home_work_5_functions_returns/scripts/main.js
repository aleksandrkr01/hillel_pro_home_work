"use strict"
var task1_1 = document.querySelector(".btn-1-1");
var task1_2 = document.querySelector(".btn-1-2");
var task2 = document.querySelector(".btn-2");
var task3_1 = document.querySelector(".btn-3-1");
var task3_2 = document.querySelector(".btn-3-2");
var task3_3 = document.querySelector(".btn-3-3");


task1_1.addEventListener("click", getEvenCycle);
task1_2.addEventListener("click", getEvenRec);
task2.addEventListener("click", function() {
	console.log(sumOrFunc());
});
task3_1.addEventListener("click", start);
task3_2.addEventListener("click", filter);
task3_3.addEventListener("click", clearCatalog);

function isNumber(num) {
	return !isNaN(parseFloat(num)) && isFinite(num);
};

// 1. Пользователь вводит число n, которое больше 0. Напишите функцию, которая выводит в консоль все 
// чётные числа из последовательности от n до 0. Решите задачу двумя способами: рекурсией и циклом. 
// Какой способ легче/лучше? Почему?
function getEvenCycle() {
	var inputNumber = Number(prompt("Введите число больше нуля"));
	if(inputNumber>0) {
		for(var i=inputNumber; i>=0; i--) {
			if(i%2===0) {
				console.log(i);
			};
		};
	}
	else {
		console.log("Вы не ввели число больше нуля");
	};
};
function getEvenRec() {
	var inputNumber = Number(prompt("Введите число больше нуля"));
	var flag = inputNumber>0;
	function getNumbers(number) {
		if(number>=0 && flag) {
			(number%2===0) ? console.log(number): console.log(number-1);
			getNumbers(number-2);
		}
		else if(!flag) {
			console.log("Вы не ввели число больше нуля");
		};
	};
	getNumbers(inputNumber);
};
// Как по мне, легче для понимания и написания метод с циклом, хотя и любое цикличиское
// действие моно представить рекурсией.
// По моему субъективному мнению в цикле for более наглядно видно условие изменения цикла и его 
// проще переделать с условия от n до 0 на условие от 0 до n (поменяв условия в цикле). Для рекурсии
// же, такое изменение потребует переменной-счётчика с изменением аргумента рекурсивной функции 
// (counter-2), изменением вывода в консоль number на counter. Хотя фактор сложности переопределения
// касается только этого алгоритма.
// Ещё ограничение рекурсии в том, что есть стек вызова функций, который может переполниться.
// Также, при вычислении больших чисел цикл справляется быстрее.
// И по Кантору: "Любая рекурсия может быть переделана в цикл. Как правило, вариант с циклом будет 
// эффективнее. Но переделка рекурсии в цикл может быть нетривиальной, особенно когда в функции, в 
// зависимости от условий, используются различные рекурсивные подвызовы, когда ветвление более 
// сложное."

// 2. Дана функция:
// function f (a, b, c) {
// function sum (a, b) {
// retur a + b;
// }
// }
// Перепишите её следующим образом:
// 2.1. Если агрументы a и b не переданы, они равны по умолчанию 2 и 3 соответсвенно.
// 2.2. Если аргумент c передан и он является функцией, то он выполняется после вызова функции sum.
// 2.4. Функция f должны возвращать результат функции аргумента c, если он есть, либо результат функции sum.
function sumOrFunc(a, b, c) {
	(typeof a==="undefined") ? a = 2: a;
	(typeof b==="undefined") ? b = 3: b;
	function sum(a, b) {
		return a + b;
	};
	if(typeof c === "function") c();
	return (typeof c==="undefined") ? sum(a, b): c();
};
// 3. Пользователь вводит в prompt() поочерёдно индекс (от 0 до 10, требуется проверка ввода), название 
// товара, его цену и категорию. Напишите функцию для сбора информации о товарах в объект, где свойствами 
// выступают индексы товаров, а значениями объект с именем, ценой и категорией.
// {
// 0: {
// name: 'pen',
// price: 2,
// category: pens
// }
// }
// Напишите функцию, которая фильтрует товары по категории и возращает новый объект только с товарами 
// выбранной категории, известно, что в объекте их не больше десяти.
// Напишите функцию, которая выводит в консоль суму цен всех товаров определённой категории. Если сумма 
// превышает 5, то выводится сумма со скидкой в 12% и пользователя получает уведомление об этом.

// Объект каталога/корзины
var catalog = {};
// Заранее извеняюсь за одинаковые название переменных-параметров index, product, price, category, cart,
// моя фантазия меня подводит
function start() {
	var index = prompt("Введите номер товара от 0 до 10");
	if (index === null) return;
	var product = prompt("Введите название товара");
	if (product === null) return;
	var price = prompt("Введите цену товара");
	if (price === null) return;
	price = Number(price).toFixed(2);
	var category = prompt("Введите категорию товара");
	if (category === null) return;
	fillObject(index, product, price, category, catalog);
	return catalog;
};
// Функция заполнения каталога/корзины
function fillObject(index, product, price, category, cart) {	
	var cartDeepnes = 10;
	if(checkTotalValidity(index, product, price, category, cart)) {
		cart = setObject(index, product, price, category, cart);	
	}
	else {
		alert("Товар не добавлен");
	};
	addProduct(cartDeepnes, cart);
	return cart;
};
// Настройка структуры
function setObject(index, product, price, category, cart) {
	cart[index] = {};
	cart[index].product = product;
	cart[index].price = price;
	cart[index].category = category;
	return cart;
};
// Проверка валидности всех полей
// Можно все проверки объеденить в одну функцию, либо отдельная проверка в отдельной функции со своим alertом,
// и передать их через операторы && / ||, но мне показалось так более наглядно, что проверку общей валидности мы 
// вносим в отдельную функцию, которая проверяет валидность каждого элемента. Так, если у нас добавится плюс одно 
// поле, нам не нужно будет менять функцию заполнения каталога
function checkTotalValidity(index, product, price, category, cart) {
	if(!isValidIndex(index)) {
		alert("Индекс должен быть числом от 0 до 10");
		return false;
	}
	if (!isIndexUnique(index, cart)) {
		alert("Индекс товаре не уникален, повторите ввод");
		return false;
	}
	if(!isValidText(product)) {
		alert("Проверьте ввод наименования товара");
		return false;
	}
	if(!isValidPrice(price)) {
		alert("Проверьте ввод цены");
		return false;
	}
	if(!isValidText(category)) {
		alert("Проверьте ввод категории");
		return false;
	}
	return true;
};
// Проверка валидности индекса
function isValidIndex(index) {
	return index>=0 && index<=10 && isNumber(index);
};
// Проверка уникальности индекса
// Можно объеденить в одну функцию с проверкой валидности, но нужно разделение для вывода отдельного сообщения пользователю
function isIndexUnique(index, cart) {
	for(var key in cart) {
		if(index == key) {
			return false;
		} 
	}
	return true;
};
// Проверка валидости цены
function isValidPrice(price) {
	return price>0 && isNumber(price);
};
// Проверка валидности текстовых полей
function isValidText(text) {
	return text !== "";
};
// Запрос ввода дополнителного продукта в зависимости от заполненности корзины
function addProduct(maxCartLenth, cart) {
	var addFlag = confirm("Добавить ещё товар?");
	if(addFlag && Object.keys(cart).length<maxCartLenth) {
		start();
	}
	else if (Object.keys(cart).length>=maxCartLenth){
		alert("Карзина переполнена");
	}
	else {
		console.log(cart);
	};
};
function filter(category) {
	var filteredCatalog = {};
	category = prompt("Введите категорию для фильтрования");
	if(category === null) return;
	for(var key in catalog) {
		if(catalog[key].category.toLowerCase() == category.toLowerCase()) {
			filteredCatalog[key] = catalog[key];
		};
	};
	console.log(filteredCatalog);
	showDiscount(filteredCatalog);
	return filteredCatalog;
};
function showDiscount(cart) {
	var discountPercent = 12;
	var totalCost = calculateTotal(cart);
	var discountedCost = (totalCost-totalCost*discountPercent/100).toFixed(2);
	var massage = "";
	if(totalCost>5) {
		massage = `Вы набрали товара на сумму ${totalCost} грн. Ваша скидка ${discountPercent}%. Итоговая стоимость: ${discountedCost} грн.`;
	}
	else {
		massage = `Может стоит повысить цены для получения скидки?`;
	};
	console.log(massage);
};
function calculateTotal(card) {
	var totalCost = 0;
	for(var key in card) {
		totalCost += +card[key].price;
	};
	return totalCost;
}
function clearCatalog() {
	catalog = {};
}