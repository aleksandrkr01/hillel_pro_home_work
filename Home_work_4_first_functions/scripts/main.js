"use strict"
var task1 = document.querySelector(".btn-1");
var task2 = document.querySelector(".btn-2");
var task3 = document.querySelector(".btn-3");
var task4 = document.querySelector(".btn-4");
var task5 = document.querySelector(".btn-5");

task1.addEventListener("click", printLessThenSQRT);
task2.addEventListener("click", isPrime);
task3.addEventListener("click", getMaxMin);
task4.addEventListener("click", getFruits);
task5.addEventListener("click", countSavings);
// Спасибо Кантору за облегчение жизни этой функцией)
function isNumber(num) {
	return !isNaN(parseFloat(num)) && isFinite(num);
};
function printLessThenSQRT() {
	var inputNumber = Number(prompt("Введите число больше либо равное нулю"));
	var minLimit = 1;
	var maxLimit = 100;
	if((isNumber(inputNumber) && inputNumber >= 0) || inputNumber == null) {
		for(var i = minLimit; i <= maxLimit; i++) {
			if(i < Math.sqrt(inputNumber)) {
				console.log(i);
			}
		}
	}
	else {
		alert("Тики, введите число больше либо равное нулю");
		printLessThenSQRT();
	}
};
function isPrime() {
	var inputNumber = prompt("Введите натуралное число больше либо равное нулю");
	var result = "простое";
	if(inputNumber > 1 && isNumber(inputNumber) && parseInt(inputNumber) == inputNumber) {
		for(var i = 2; i <= inputNumber/2; i++) {
			if(inputNumber%i===0 && inputNumber!==i) {
				result = "комплексное"
			}
		}
	}
	else if(inputNumber == 1) {
		result = "не простое, не комплексное";
	}
	else if(inputNumber == null || inputNumber == "") {
		return;
	}
	else {
		result = "точно натуральное число?"
	}
	console.log(`Число ${inputNumber} - ${result}`);
};
function getMaxMin() {
	var numberOfPrompts = 3;
	var max = -Infinity;
	var min = Infinity;
	var inputNumber;
	var massage;
	var sign;
	for(var i = 1; i<=numberOfPrompts; i++) {
		inputNumber = prompt(`Введите число ${i}`);
		if(isNumber(inputNumber)) {
			+inputNumber > max ? max = +inputNumber: max = max;
			+inputNumber < min ? min = +inputNumber: min = min;
		}
		else {
			massage = `Вы неверно ввели число ${i}. Начинаем заново`;
			console.log(massage);
			return getMaxMin()
		} 
	};
	sign = prompt("Введите знак +/-");
	switch (sign) {
		case "+": 
			massage = max;
			break;
		case "-": 
			massage = min;
			break;
		default:
			massage = "Вы ввели неверный знак";
	};
	console.log(massage);
};
function getFruits() {
	var massage = "";
	var flag = true;
	while(flag) {
		var fruit = prompt("Введите фрукт");
		if (fruit !== null) {
			massage += fruit + ", ";
		}
		else {
			massage = massage.slice(0, -2) + ".";
			if (massage==".") {
				massage = "Вы не ввели фрукты";
			};
			console.log(massage);
			flag = false;
		}
	}
}
function countSavings() {
	var currentCourse = 2772.4;
	var amountOfSavings = Number(prompt("Введите сумму сбережений в долларах"));
	var newCourse = Number(prompt("Введите текущий курс валют"));
	var currentSavingsUAH = amountOfSavings*currentCourse/100;
	var newSavingsUAH = amountOfSavings*newCourse/100;
	var difference = newSavingsUAH - currentSavingsUAH;
	difference = difference.toFixed(2);
	var massage = "";
	if(difference>=0) {
		massage = `Вы заработали ${difference} гривен. Ваши сбережения в гривне: ${newSavingsUAH} гривен.`;
	}
	else if(difference<0) {
		massage = `Вы потеряли ${difference} гривен. Ваши сбережения в гривне: ${newSavingsUAH} гривен.`;
	}
	else massage = "Вы ввели не числа, проверьте данные";
	console.log(massage);
}
